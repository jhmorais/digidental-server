import { Injectable } from '@nestjs/common';
import { NestSchedule, Cron } from 'nest-schedule';
import { Aporte } from 'src/models/Aporte.entity';
import { AporteController } from 'src/controllers/aporte/aporte.controller';
import { getRepository, getConnection } from 'typeorm';

@Injectable()
export class CronService extends NestSchedule {

    private aporteController : AporteController = new AporteController();

    @Cron('1 0 0 * * *') // Run every middle night
    // @Cron('* * * * * *') // Run every second
    scheduledJob() {
        var moment = require('moment');
        var dataInicial = moment(new Date()).format('DD-MM-YYYY hh:mm:ss');
        // console.info('[Scheduler]: scheduled jobs has been started - ' + dataInicial);
        // console.info(this.atualizarSaldoAportes());
        // console.info(this.aporteController.atualizarSaldoAportes());
        // this.aporteController.atualizarSaldoAportes();
    }

}