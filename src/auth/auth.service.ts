import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { UsuarioController } from 'src/controllers/usuario/usuario.controller';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService
  ) {}

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService.findOne(username);
    
    if(user == undefined){
      return null;
    }
    
    const bcrypt = require('bcrypt');
    const match = await bcrypt.compare(pass, user.senha);

    if (user && match) {
      const { senha, ...result } = user;
      return result;
    }
    return null;
  }

  async login(user: any) {
    var userLogin = await this.validateUser(user.userName, user.password)
    if(userLogin !== null && userLogin.id !== null){
      const payload = { 
        username: user.username, 
        sub: {
          userId: userLogin.id, 
          tipoUsuario: userLogin.tipoUsuario, 
        } 
      };
      return {
        access_token: this.jwtService.sign(payload),
      }
    }else{
      return {status: 500, message: "Usuário não autorizado"}
    }
  }

  async validateToken(token: string) {
    return this.jwtService.verify(token);
  }

}