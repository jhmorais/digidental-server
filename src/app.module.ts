import { Module, MiddlewareConsumer } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm'
import { CidadeController } from './controllers/cidade/cidade.controller';
import { UfController } from './controllers/uf/uf.controller';
import { PessoaJuridicaController } from './controllers/pessoaJuridica/pessoaJuridica.controller';
import { PessoaFisicaController } from './controllers/pessoaFisica/pessoaFisica.controller';
import { PessoaController } from './controllers/pessoa/pessoa.controller';
import { UsuarioController } from './controllers/usuario/usuario.controller';
import { ConsultorController } from './controllers/consultor/consultor.controller';
import { AporteController } from './controllers/aporte/aporte.controller';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { GestorController } from './controllers/gestor/gestor.controller';
import { LoggerMiddleware } from './logger.middleware';
import { CronService } from "./services/cron.service";
import { ScheduleModule } from 'nest-schedule';
import { TotalRendimentoController } from './controllers/totalRendimento/totalRendimento.controller';
import { ComissaoController } from './controllers/comissao/comissao.controller';
import { PacienteController } from './controllers/paciente/paciente.controller';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    AuthModule, 
    UsersModule,
    ScheduleModule.register(),
  ],
  controllers: [
    AppController,
    CidadeController, 
    UfController, 
    PessoaController, 
    UsuarioController,
    ConsultorController,
    AporteController,
    GestorController,
    PessoaJuridicaController,
    PessoaFisicaController,
    TotalRendimentoController,
    ComissaoController,
    PacienteController
  ],
  providers: [AppService, CronService],
})
export class AppModule { 
  // configure(consumer: MiddlewareConsumer) {
  //   consumer
  //     .apply(LoggerMiddleware)
  //     .forRoutes(
  //       AppController,
  //       CidadeController, 
  //       PessoaController, 
  //       UsuarioController,
  //       ConsultorController,
  //       AporteController,
  //       GestorController,
  //       PessoaFisicaController,
  //       PessoaJuridicaController,
  //       TotalRendimentoController,
  //       ComissaoController
  //     );
  // }
}
