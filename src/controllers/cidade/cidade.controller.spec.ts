import { Test, TestingModule } from '@nestjs/testing';
import { CidadeController } from './cidade.controller';
import { Cidade } from 'src/models/Cidade.entity';
import { Uf } from 'src/models/Uf.entity';

describe('Cidade Controller', () => {
  let controller: CidadeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CidadeController],
    }).compile();

    controller = module.get<CidadeController>(CidadeController);
  });

  describe('cidadePorNome', () => {
    it('should return an city had name', async () => {
      const result = {
        id: 1,
        nome: 'Goiânia',
        ufId: 1
      };
      let cidadeModel = new Cidade()
      cidadeModel.id = 1
      cidadeModel.nome = 'Goiânia'
      cidadeModel.ufId = 1
      cidadeModel.uf = new Uf()
      jest.spyOn(controller, 'cidadePorNome').mockImplementation(() => cidadeModel);

      expect(await controller.cidadePorNome('Goiânia')).toBe(result);
    });
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
