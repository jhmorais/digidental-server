import { Controller, Get, Post, Param, Body, Headers } from '@nestjs/common';
import { Cidade } from 'src/models/Cidade.entity';
import { getConnection, getRepository } from 'typeorm';

@Controller('cidade')
export class CidadeController {

    @Get('/nomeCidade/:nome')
    async cidadePorNome(@Param('nome') nome : string) {

        const cidade = await this.getCidadePorNome(nome)

        return {
            cidade
        }

    }

    private async getCidadePorNome(nome : string) {
        // return await Cidade.find({
        //     where: {
        //         nome
        //     },
        //     order: {
        //         nome: 'DESC',
        //     }
        // });
        return await getRepository(Cidade)
        .createQueryBuilder("cidade")
        .where('cidade.nome ilike :nome', {nome: '%' + nome + '%'}).getMany();
    }

    @Get('/cidadePorEstado/:idEstado')
    async cidadePorEstado(@Param('idEstado') idEstado : number) {

        const cidades = await this.getCidadePorEstado(idEstado)
        return {
            cidades
        }

    }

    private async getCidadePorEstado(idEstado : number) {
        return await getRepository(Cidade)
            .createQueryBuilder("cidade")
            .where('cidade.ufId = :idEstado', {idEstado: idEstado})
            .getMany();
    }

    @Get()
    async index() {

        const cidades = await this.cidades()

        return {
            cidades
        }

    }

    private async cidades() {
        return await Cidade.find({
            // where: {
            //     id
            // },
            order: {
                nome: 'DESC',
            }
        });
    }
    
    @Get(':idCapital')
    async capital(@Param('idCapital') idCapital) {

        const capital = await this.getCapital(idCapital)

        return {
            capital
        }

    }

    private async getCapital(id: number) {
        return await Cidade.findOne({
            where: {
                id
            },
            order: {
                nome: 'DESC',
            }
        });
    }

    @Post('createCidade')
    async createCidade(@Body() message: any, @Headers() headers: any){

        //save default of entity
        var cidadeModel = new Cidade();
        cidadeModel.nome = message.nome;
        cidadeModel.ufId = message.ufId;
        getRepository(Cidade).save(cidadeModel);

        return message;
    }

    @Post('createCidadeQueryBuilder')
    async createCidadeQueryBuilder(@Body() message: any){
        //save com query builder
        await getConnection()
        .createQueryBuilder()
        .insert()
        .into(Cidade)
        .values([
            { nome: message.nome, ufId: message.ufId }
         ])
        .execute();

        return message;
    }
    
}
