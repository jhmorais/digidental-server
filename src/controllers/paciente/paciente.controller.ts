import { Controller, Get, Post, Body, Headers, Param } from '@nestjs/common';
import { Pessoa } from '../../models/Pessoa.entity';
import { getConnection, getRepository, Raw, getManager } from 'typeorm';
import { getEntityManagerToken } from '@nestjs/typeorm';
import { Usuario } from 'src/models/Usuario.entity';
import { PessoaFisica } from 'src/models/PessoaFisica.entity';
import { Paciente } from 'src/models/Paciente.entity';
import { Endereco } from 'src/models/Endereco.entity';

@Controller('paciente')
export class PacienteController {

    @Get('/pacientePorNome/:nome')
    async pacientePorNome(@Param('nome') nome : string) {

        const paciente = await this.getPacientePorNome(nome)
        return {
            paciente
        }
    }
    
    @Get('/listarPacientes')
    async listarPacientes() {

        const pacientes = await this.getPacientes()
        return {
            pacientes
        }
    }

    private async getPacientes() {
        return await getRepository(Paciente)
        .createQueryBuilder("paciente")
        .leftJoinAndSelect("paciente.endereco", "endereco")
        .leftJoinAndSelect("endereco.cidade", "cidade")
        .leftJoinAndSelect("paciente.enderecoProfissional", "enderecoProfissional")
        .leftJoinAndSelect("enderecoProfissional.cidade", "cidadeProfissional")
        .leftJoinAndSelect("paciente.naturalidadeCidade", "cidadeNaturalidade")
        .where("paciente.excluido = false")
        .orderBy("paciente.nome", "ASC")
        .getMany();
    }

    @Get('/listaPessoasSemUsuario')
    async listaPessoasSemUsuario() {

        const pessoas = await this.getPessoasSemUsuario()
        return {
            pessoas
        }
    }

    private async getPessoasSemUsuario() {
        var listaPessoas = await getRepository(Pessoa)
        .createQueryBuilder("pessoa")
        .leftJoinAndSelect("pessoa.pessoaFisica", "pessoaFisica")
        .leftJoinAndSelect("pessoa.usuario", "usuario")
        .where("usuario.id is null")
        .orderBy("pessoa.nome", "ASC")
        .getMany();

        // return listaPessoas
        return listaPessoas.filter(pes => pes.usuario == null || pes.usuario.excluido == true)
    }

    private async getPacientePorNome(nome : string) {

        return await getRepository(Paciente)
        .createQueryBuilder("paciente")
        .leftJoinAndSelect("paciente.endereco", "endereco")
        .leftJoinAndSelect("endereco.cidade", "cidade")
        .where('paciente.nome ilike :nome', {nome: '%' + nome + '%'}).getMany();

    }
    
    @Get('pacientePorId/:idPaciente')
    async pacientePorId(@Param('idPaciente') idPaciente) {
        const paciente = await this.getPacientePorId(idPaciente)

        return {
            paciente
        }

    }

    private async getPacientePorId(id: number) {
        return await getRepository(Paciente)
        .createQueryBuilder("paciente")
        .leftJoinAndSelect("paciente.endereco", "endereco")
        .leftJoinAndSelect("endereco.cidade", "cidade")
        .leftJoinAndSelect("paciente.enderecoProfissional", "enderecoProfissional")
        .leftJoinAndSelect("enderecoProfissional.cidade", "cidadeProfissional")
        .leftJoinAndSelect("paciente.naturalidadeCidade", "cidadeNaturalidade")
        .where("paciente.id = :id", {id: id})
        .getOne();
    }

    private async getPessoaPorUsuarioId(usuarioId: number) {
        return await getRepository(Pessoa)
        .createQueryBuilder("pessoa")
        .leftJoinAndSelect("pessoa.pessoaFisica", "pessoaFisica")
        .leftJoinAndSelect("pessoa.usuario", "usuario")
        .leftJoinAndSelect("pessoa.cidade", "cidade")
        .leftJoinAndSelect("cidade.uf", "uf")
        .where("usuario.id = :usuarioId", {usuarioId: usuarioId})
        .getOne();
    }

    private bindPaciente(paciente: any, cpf: string){
        var pacienteModel = new Paciente();
        if(paciente.id != null)
            pacienteModel.id = paciente.id 
        pacienteModel.nome = paciente.nome;
        pacienteModel.cpf = cpf;
        pacienteModel.profissao = paciente.profissao;
        pacienteModel.telefone = paciente.telefone;
        pacienteModel.endereco = paciente.endereco;
        pacienteModel.email = paciente.email;
        pacienteModel.celular = paciente.celular;
        pacienteModel.sexo = paciente.sexo;
        pacienteModel.conjugue = paciente.conjugue;
        pacienteModel.rg = paciente.rg != '' ? paciente.rg : null;
        pacienteModel.orgaoExpeditor = paciente.orgaoExpeditor;
        pacienteModel.estadoCivil = paciente.estadoCivil != '' ? paciente.estadoCivil : null;
        pacienteModel.dataNascimento = paciente.dataNascimento != '' ? paciente.dataNascimento : null
        pacienteModel.nacionalidade = paciente.nacionalidade;
        pacienteModel.naturalidadeCidadeId = paciente.naturalidadeCidadeId != '' ? paciente.naturalidadeCidadeId : null;
        pacienteModel.excluido = paciente.excluido;
        pacienteModel.parentesco = paciente.parentesco;
        pacienteModel.contatoEmergencia = paciente.contatoEmergencia != '' ? paciente.contatoEmergencia : null;
        return pacienteModel;
    }

    @Post('salvarPaciente')
    async salvarPaciente(@Body() message: any, @Headers() headers: any){
        var enderecoModel = new Endereco();
        if(message.endereco.id != null)
            enderecoModel.id = message.endereco.id
        enderecoModel.cep = message.endereco.cep != '' ? message.endereco.cep.replace(/\D/g, '') : null
        enderecoModel.setor = message.endereco.setor
        enderecoModel.numero = message.endereco.numero != "" ? message.endereco.numero : null
        enderecoModel.cidadeId = message.endereco.cidadeId != "" ? message.endereco.cidadeId : null
        enderecoModel.logradouro = message.endereco.logradouro
        enderecoModel = await getRepository(Endereco).save(enderecoModel)
        
        if(message.enderecoProfissional != undefined && message.enderecoProfissional.cep != undefined && message.enderecoProfissional.cep != ""){
            var enderecoProfissionalModel = new Endereco();
            if(message.enderecoProfissional.id != null)
                enderecoProfissionalModel.id = message.enderecoProfissional.id
            enderecoProfissionalModel.cep = message.enderecoProfissional.cep != '' ? message.enderecoProfissional.cep.replace(/\D/g, '') : null
            enderecoProfissionalModel.setor = message.enderecoProfissional.setor
            enderecoProfissionalModel.numero = message.enderecoProfissional.numero != '' ? message.enderecoProfissional.numero : null
            enderecoProfissionalModel.cidadeId = message.enderecoProfissional.cidadeId
            enderecoProfissionalModel.logradouro = message.enderecoProfissional.logradouro
            enderecoProfissionalModel = await getRepository(Endereco).save(enderecoProfissionalModel)
        }

        var pacienteModel = this.bindPaciente(message.paciente, message.cpf)
        var responsavelPaciente = message.cpfResponsavel == "" ? null : this.bindPaciente(message.responsavel, message.cpfResponsavel)
        pacienteModel.enderecoId = enderecoModel.id
        pacienteModel.enderecoProfissionalId = enderecoProfissionalModel != undefined ? enderecoProfissionalModel.id : null
        
        if(responsavelPaciente != null && responsavelPaciente.cpf != null){
            responsavelPaciente.enderecoId = enderecoModel.id
            responsavelPaciente = await getRepository(Paciente).save(responsavelPaciente)
            pacienteModel.responsavelPacienteId = responsavelPaciente.id
        }

        pacienteModel = await getRepository(Paciente).save(pacienteModel)

        return pacienteModel;
    }

    @Post('editarPessoaPerfil')
    async editarPessoaPerfil(@Body() message: any, @Headers() headers: any){
        var pessoaModel = new Pessoa();
        pessoaModel.id = message.pessoa.id;
        pessoaModel.nome = message.pessoa.nome;
        pessoaModel.cidadeId = message.pessoa.cidadeId;
        pessoaModel.profissao = message.pessoa.profissao;
        pessoaModel.telefone = message.pessoa.telefone.replace(/\D/g, '');
        pessoaModel.endereco = message.pessoa.endereco;
        pessoaModel.cep = message.pessoa.cep.replace(/\D/g, '');
        pessoaModel.complemento = message.pessoa.complemento;
        pessoaModel.numero = message.pessoa.numero;
        pessoaModel.bairro = message.pessoa.bairro;
        pessoaModel.email = message.pessoa.email;
        pessoaModel.status = message.pessoa.status;
        pessoaModel.estadoCivil = message.pessoa.estadoCivil;

        pessoaModel = await getRepository(Pessoa).save(pessoaModel)

        var pessoaFisicaModel = new PessoaFisica();
        pessoaFisicaModel.pessoaId = pessoaModel.id;
        pessoaFisicaModel.cpf = message.cpf;
        pessoaFisicaModel = await getRepository(PessoaFisica).save(pessoaFisicaModel);

        return pessoaModel;
    }

    @Post('deletePessoa')
    async deletePessoa(@Body() message: any, @Headers() headers: any){
        
        var usuario = await getRepository(Usuario)
        .createQueryBuilder("usuario")
        .leftJoinAndSelect("usuario.pessoa", "pessoa")
        .where('usuario.pessoaId = :pessoaId', {pessoaId: message.pessoaId})
        .andWhere("usuario.excluido = :excluido", {excluido: false})
        .getOne();
        
        await this.excluirPessoaFisica(message);
        
        if(usuario == null){
            return await getConnection()
                .createQueryBuilder()
                .delete()
                .from(Pessoa)
                .where("id = :id", { id: message.pessoaId })
                .execute();
        }else{
            return {
                status: "Fail",
                message: "Pessoa selecionada para excluir possui um usuário vinculado"
            }
        }

    }

    async excluirPessoaFisica(message : any){
        var pessoaFisicaModel = await getRepository(PessoaFisica)
        .createQueryBuilder("pessoaFisica")
        .where('pessoaFisica.pessoaId = :pessoaId', {pessoaId: message.pessoaId})
        .getOne();

        if(pessoaFisicaModel != null){
            await getConnection()
                .createQueryBuilder()
                .delete()
                .from(PessoaFisica)
                .where("id = :id", { id: pessoaFisicaModel.id })
                .execute();
        }
    }
    
    @Post('editPessoa')
    async editPessoa(@Body() message: any, @Headers() headers: any){
        await getRepository(Pessoa).update(message.pessoaId, {
            nome: message.nome, 
            email: message.email,
            telefone: message.telefone
        });

        var pessoaAtualizada = await getRepository(Pessoa)
            .createQueryBuilder("pessoa")
            .leftJoinAndSelect("pessoa.pessoaFisica", "pessoaFisica")
            .where("pessoa.id = :pessoaId", {pessoaId: message.pessoaId})
            .getOne();
        
        var pessoaFisicaModel = pessoaAtualizada.pessoaFisica != null ? pessoaAtualizada.pessoaFisica : new PessoaFisica();
        pessoaFisicaModel.pessoaId = pessoaAtualizada.id;
        pessoaFisicaModel.cpf = message.cpf;
        pessoaFisicaModel.nascimento = message.dataNascimento;
        pessoaFisicaModel = await getRepository(PessoaFisica).save(pessoaFisicaModel);

        return pessoaAtualizada;
    }
    
}