import { Controller, Get, Post, Body, Headers, Param } from '@nestjs/common';
import { getRepository, getConnection, Brackets } from 'typeorm';
import { Usuario } from 'src/models/Usuario.entity';
import { TotalRendimento } from 'src/models/TotalRendimento.entity';
import { Aporte } from 'src/models/Aporte.entity';
import { AporteController } from '../aporte/aporte.controller';
import { TipoAporteEnum } from 'src/enums/tipoAporte.enum';
import { StatusAporteEnum } from 'src/enums/statusAporte.enum';
import { TipoUsuarioEnum } from 'src/enums/tipoUsuario.enum';
import { ComissaoController } from '../comissao/comissao.controller';

@Controller('totalRendimento')
export class TotalRendimentoController {

    // private readonly aporte:AporteController = new AporteController()
    private readonly comissaoController:ComissaoController = new ComissaoController()
    
    @Get('/listaRendimentosTotais/')
    async listaRendimentosTotais() {

        const rendimentosTotais = await this.getTodosRendimentos()
        return {
            rendimentosTotais
        }
    }

    private async getTodosRendimentos() {
        return await getRepository(TotalRendimento)
        .createQueryBuilder("totalrendimento")
        .orderBy("totalrendimento.dataLancamento", "DESC").getMany();
    }
    
    @Get('/getRendimento/:id')
    async getRendimento(@Param('id') id : number) {

        const totalRendimento = await this.getRendimentoPorId(id)
        return {
            totalRendimento
        }
    }

    private async getRendimentoPorId(id : number) {
        return await getRepository(TotalRendimento)
        .createQueryBuilder("totalrendimento")
        .where("totalrendimento.id = :id", {id})
        .orderBy("totalrendimento.dataLancamento", "DESC").getOne();
    }

    async getUltimaTaxa(){
        return getRepository(TotalRendimento)
        .createQueryBuilder("totalrendimento")
        .orderBy("totalrendimento.ano", "DESC")
        .addOrderBy("totalrendimento.mes", "DESC")
        .getOne()
    }

    private bindTotalRendimento(message: any){
        var totalRendimentoModel = new TotalRendimento();
        totalRendimentoModel.ano = message.ano;
        totalRendimentoModel.mes = message.mes;
        totalRendimentoModel.taxa = message.taxa;
        return totalRendimentoModel
    }

    @Post('createRendimento')
    async createRendimento(@Body() message: any, @Headers() headers: any){
        var totalRendimentoModel = this.bindTotalRendimento(message);
        var moment = require('moment')
        var dataAtual = moment(new Date()).format(`YYYY-MM-DD hh:mm:ss`)
        totalRendimentoModel.dataLancamento = dataAtual

        totalRendimentoModel = await getRepository(TotalRendimento).save(totalRendimentoModel)
        await this.calcularRendimentoAportesTotalRendimento()

        return totalRendimentoModel;
    }

    @Post('editarTotalRendimento')
    async editarTotalRendimento(@Body() message: any, @Headers() headers: any){
        var totalRendimentoModel = this.bindTotalRendimento(message);
        totalRendimentoModel.dataLancamento = message.dataLancamento
        totalRendimentoModel.id = message.id

        totalRendimentoModel = await getRepository(TotalRendimento).save(totalRendimentoModel)

        return totalRendimentoModel;
    }

    @Post('excluirRendimento')
    async excluirRendimento(@Body() message: any, @Headers() headers: any){
        var totalRendimentoModel = this.bindTotalRendimento(message);
        totalRendimentoModel.dataLancamento = message.dataLancamento
        totalRendimentoModel.id = message.id

        await getRepository(TotalRendimento).delete(totalRendimentoModel)
    }

    async buscarAportesAbertosEComPorcentagemSobreTaxa(){
        return await getRepository(Aporte)
            .createQueryBuilder("aporte")
            .leftJoinAndSelect("aporte.resgates", "resgate")
            .leftJoinAndSelect("aporte.conta", "conta")
            .leftJoinAndSelect("conta.cliente", "cliente")
            .leftJoinAndSelect("cliente.usuario", "usuario")
            .where("aporte.status = :status", {status: StatusAporteEnum.Ativo})
            .andWhere(new Brackets(qb => {
                qb.where("aporte.tipo = :tipo", {tipo: TipoAporteEnum.Aberto})
                .orWhere("aporte.tipo = :tipo2", {tipo2: TipoAporteEnum.SemiAberto})
            }))
            .printSql()
            .getMany()
    }

    async buscarAportesComissao(){
        return await getRepository(Aporte)
            .createQueryBuilder("aporte")
            .leftJoinAndSelect("aporte.resgates", "resgate")
            .leftJoinAndSelect("aporte.conta", "conta")
            .leftJoinAndSelect("conta.cliente", "cliente")
            .leftJoinAndSelect("cliente.usuario", "usuario")
            .where("aporte.status = :status", {status: StatusAporteEnum.Ativo})
            .andWhere("usuario.tipoUsuario = :tipoUsuario", {tipoUsuario: TipoUsuarioEnum.TI})
            .andWhere("aporte.tipo = :tipo", {tipo: TipoAporteEnum.Comissao})
            .getMany()
    }

    async calcularRendimentoAportesTotalRendimento(){
        var aportesAbertosEComPorcentagemSobreTaxa = await this.buscarAportesAbertosEComPorcentagemSobreTaxa()
        var aportesComissao = await this.buscarAportesComissao()
        var taxaTotalRendimento = await this.getUltimaTaxa()
        var moment = require('moment')
        let dataAtual = moment(new Date())
        
        await this.calcularRendimentoAportesAbertosESemiAbertos(aportesAbertosEComPorcentagemSobreTaxa, dataAtual, taxaTotalRendimento)
        //TODO rendimento das comissoes 
        // await this.calcularRendimentosAportesComissao(aportesComissao, taxaTotalRendimento, dataAtual)
    }

    private async calcularRendimentoAportesAbertosESemiAbertos(aportesAbertosEComPorcentagemSobreTaxa : Array<Aporte>, dataAtual : any, taxaTotalRendimento : TotalRendimento){
        var moment = require('moment')
        for(let [index, aporte] of aportesAbertosEComPorcentagemSobreTaxa.entries()){
            let dataAporte = moment(aporte.dataBase, 'YYYY-MM-DD')
            let diferencaMeses = dataAtual.diff(dataAporte, 'months')
            let porcentagemRendimento = aporte.conta.cliente.usuario.tipoUsuario == TipoUsuarioEnum.TI ? 1 : Number(aporte.conta.cliente.porcentagemRendimento)/100
            let comissaoTI = aporte.conta.cliente.usuario.tipoUsuario == TipoUsuarioEnum.TI ? 0 : 1
            let diasDoMesPassado = moment(`${dataAtual.year()}-${dataAtual.month()+1}`, 'YYYY-MM').subtract(1, 'months').daysInMonth()
            let taxaRendimentoDiario = Math.pow( (1+(Number(taxaTotalRendimento.taxa)/100)) , 1/diasDoMesPassado)
            
            if(diferencaMeses > 0 && diferencaMeses > aporte.meses){
                if(aporte.resgates != null && aporte.resgates.length > 0){ // aporte aberto com resgates durante o mês corrente
                    let somaRendimentoResgates = 0
                    let dataUltimoResgate = aporte.resgates[0].data

                    for(let [index, resgate] of aporte.resgates.entries()){
                        somaRendimentoResgates += (resgate.valorAporteAntesDoResgate * Math.pow(taxaRendimentoDiario, moment(resgate.data).date())) - Number(resgate.valorAporteAntesDoResgate)
                        dataUltimoResgate = moment(dataUltimoResgate).diff(resgate.data) < 0 ? dataUltimoResgate : resgate.data
                    }

                    let periodoAposResgates = diasDoMesPassado - moment(dataUltimoResgate).date()
                    let rendimentoPeriodoAposResgates = (Math.pow(taxaRendimentoDiario, periodoAposResgates) * Number(aporte.totalRendimento)) - Number(aporte.totalRendimento)
                    aporte.totalRendimento = Number(rendimentoPeriodoAposResgates) + Number(somaRendimentoResgates) + Number(aporte.totalRendimento)
                }else{// aporte aberto sem resgate 
                    aporte.totalRendimento = Number(aporte.totalRendimento) * (1 + (Number(taxaTotalRendimento.taxa * porcentagemRendimento) - comissaoTI)/100 ) //1% de comissao para TI
                }
                
                aporte.saldoRendimento = aporte.totalRendimento
                aporte.dataInicial = moment(aporte.dataInicial).add(1, 'months')
                aporte.meses = Number(aporte.meses) + 1
                await this.comissaoController.criarComissoes(aporte)
            }else if(moment(aporte.dataInicial).date() != 1){// para datas diferentes do dia 01/mes/ano
                var valorRendimentoMesCheio = Number(aporte.totalRendimento) * (1 + ( Number(taxaTotalRendimento.taxa) / 100 ))
                this.calcularRendimentoPeriodoInferiorAUmMes(dataAtual, aporte, taxaTotalRendimento)
                await this.comissaoController.criarComissaoTIPorcentagemProporcional(aporte, valorRendimentoMesCheio)
            }

            await this.updateAporte(aporte) 
        }
    }

    /**
     * @deprecated
     * @param aportesComissao 
     * @param taxaTotalRendimento 
     * @param dataAtual 
     */
    private async calcularRendimentosAportesComissao(aportesComissao : Array<Aporte>, taxaTotalRendimento : TotalRendimento, dataAtual : any){
        var moment = require('moment')
        for(let [index, aporte] of aportesComissao.entries()){
            let dataAporte = moment(aporte.dataInicial, 'YYYY-MM-DD')
            let diferencaMeses = dataAtual.diff(dataAporte, 'months')

            if(diferencaMeses > 0 && diferencaMeses > aporte.meses){
                aporte.totalRendimento = Number(aporte.totalRendimento) * (1 + Number(taxaTotalRendimento.taxa)/100 )
                aporte.saldoRendimento = aporte.totalRendimento
                aporte.meses = Number(aporte.meses) + 1
                
                if(aporte.conta.cliente.usuario.tipoUsuario == TipoUsuarioEnum.Consultor){
                    await this.comissaoController.criarComissoes(aporte)
                }
            }else if(moment(aporte.dataInicial).date() != 1){// para datas diferentes do dia 01/mes/ano
                var valorRendimentoMesCheio = Number(aporte.totalRendimento) * (1 + ( Number(taxaTotalRendimento.taxa) / 100 ))
                aporte = this.calcularRendimentoPeriodoInferiorAUmMes(dataAtual, aporte, taxaTotalRendimento)
                
                if(aporte.conta.cliente.usuario.tipoUsuario == TipoUsuarioEnum.Consultor){
                    await this.comissaoController.criarComissaoTIPorcentagemProporcional(aporte, valorRendimentoMesCheio)
                }
            }
            await this.updateAporte(aporte)    
        }
    }

    private calcularRendimentoPeriodoInferiorAUmMes(dataAtual, aporte:Aporte, taxaTotalRendimento:TotalRendimento){
        var moment = require('moment')
        var diasDoMesPassado = moment(`${dataAtual.year()}-${dataAtual.month()+1}`, 'YYYY-MM').subtract(1, 'months').daysInMonth()
        let diasCorridosDoAporte = Number(diasDoMesPassado) - moment(aporte.dataInicial).date()
        var rendimentoDiario = 0
        let porcentagemRendimento = aporte.conta.cliente.usuario.tipoUsuario == TipoUsuarioEnum.TI ? 1 : Number(aporte.conta.cliente.porcentagemRendimento)/100

        rendimentoDiario = Math.pow((1 + ((Number(taxaTotalRendimento.taxa * porcentagemRendimento) - 1)/100)), (1/diasDoMesPassado))

        aporte.totalRendimento = Number(aporte.totalRendimento) * (Math.pow(Number(rendimentoDiario), Number(diasCorridosDoAporte)))
        aporte.saldoRendimento = aporte.totalRendimento
        aporte.dataInicial = moment(`${dataAtual.year()}-${dataAtual.month()+1}-01`, 'YYYY-MM-DD')
        return aporte
    }

    private async updateAporte(aporte : Aporte){
        await getConnection().createQueryBuilder().update(Aporte)
            .set({
                saldoRendimento: aporte.saldoRendimento,
                totalRendimento: aporte.totalRendimento,
                meses: aporte.meses,
                dataInicial: aporte.dataInicial,
            })
            .where("id = :id", { id: aporte.id })
            .execute()
    }

}
