import { Controller, Get } from '@nestjs/common';
import { Gestor } from 'src/models/Gestor.entity';
import { getRepository } from 'typeorm';

@Controller('gestor')
export class GestorController {

    @Get('/listaGestores')
    async listaGestores() {

        const gestores = await this.getGestores()
        return {
            gestores
        }
    }

    private async getGestores() {
        return await getRepository(Gestor)
        .createQueryBuilder("gestor")
        .leftJoinAndSelect("gestor.usuario", "usuario")
        .leftJoinAndSelect("usuario.pessoa", "pessoa")
        .where('gestor.excluido = false')
        .orderBy('pessoa.nome').getMany();
    }
}
