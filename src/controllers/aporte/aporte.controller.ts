import { Controller, Get, Post, Body, Headers, Param } from '@nestjs/common';
import { Usuario } from 'src/models/Usuario.entity';
import { getRepository, getConnection, Connection, Brackets } from 'typeorm';
import { Cliente } from 'src/models/Cliente.entity';
import { Aporte } from 'src/models/Aporte.entity';
import { Conta } from 'src/models/Conta.entity';
import { ComissaoController } from '../comissao/comissao.controller';
import { TipoAporteEnum } from 'src/enums/tipoAporte.enum';
import { StatusAporteEnum } from 'src/enums/statusAporte.enum';
import { TipoUsuarioEnum } from 'src/enums/tipoUsuario.enum';
import { TotalRendimento } from 'src/models/TotalRendimento.entity';
import { TotalRendimentoController } from '../totalRendimento/totalRendimento.controller';
import { Query } from '../../../node_modules/jaacoder-typeorm-query';
import { getConnectionName } from '@nestjs/typeorm';

@Controller('aporte')
export class AporteController {
    private readonly enumStatusAporte: Object = {
        "Solicitado": 1,
        "Atendido": 2,
        "Transferencia": 3,
        "Ativo": 4,
        "Inativo": 5,
        "Falha": 6,
        "Cancelado": 7,
        1: 'Solicitado',
        2: 'Atendido',
        3: 'Transferencia',
        4: 'Ativo',
        5: 'Inativo',
        6: 'Falha',
        7: 'Cancleado'
    };
    
    private readonly enumTipoOrdencao: Object = {
        "DataDecrescente": 1,
        "DataCrescente": 2,
        "MaiorValor": 3,
        "MenorValor": 4,
    };

    private readonly taxaMes = 4;
    private readonly comissaoController : ComissaoController = new ComissaoController();
    private readonly totalRendimento : TotalRendimentoController = new TotalRendimentoController();

    @Post('solicitarAporte')
    async solicitarAporte(@Body() message: any, @Headers() headers: any){
        var conta = await getRepository(Conta)
            .createQueryBuilder("conta")
            .leftJoinAndSelect("conta.cliente", "cliente")
            .leftJoinAndSelect("cliente.usuario", "usuario")
            .where('usuario.id = :id', {id: message.usuarioId})
            .getOne();

        var moment = require('moment');
        var dataInicial = moment(new Date());
        dataInicial.hour(0);
        dataInicial.minute(0);
        dataInicial.second(0);

        var aporteModel = new Aporte();
        aporteModel.valor = Number(message.valor.replace(',', '.'));
        aporteModel.contaId = conta.id;
        aporteModel.dataBase = dataInicial;
        aporteModel.dataInicial = dataInicial;
        aporteModel.saldoRendimento = message.valor;
        aporteModel.totalRendimento = message.valor;
        aporteModel.meses = 0;
        aporteModel.taxaMajoracaoUsuario = conta.cliente.rendimentoVariavel == false ? conta.cliente.usuario.taxaMajoracao : null;
        aporteModel.codigo = 0;
        aporteModel.status = this.enumStatusAporte['Transferencia'];

        if((conta.cliente.rendimentoVariavel == true) || conta.cliente.usuario.tipoUsuario == TipoUsuarioEnum.TI){
            aporteModel.tipo = TipoAporteEnum.Aberto
        }else if(conta.cliente.usuario.tipoUsuario == TipoUsuarioEnum.Cliente || conta.cliente.usuario.tipoUsuario == TipoUsuarioEnum.Consultor){
            aporteModel.tipo = TipoAporteEnum.Normal
        }
        
        aporteModel = await getRepository(Aporte).save(aporteModel);
        aporteModel.codigo = Number(aporteModel.id) + 1364;
        aporteModel = await getRepository(Aporte).save(aporteModel);
        
        return aporteModel;
    }

    @Get('/getAporte/:aporteId')
    async getAporte(@Param('aporteId') aporteId : number) {

        const aporte = await this.getAportePorId(aporteId)

        return {
            aporte
        }
    }

    private async getAportePorId(aporteId : number) {
        return await getRepository(Aporte)
            .createQueryBuilder("aporte")
            .leftJoinAndSelect("aporte.conta", "conta")
            .leftJoinAndSelect("conta.cliente", "cliente")
            .leftJoinAndSelect("cliente.usuario", "usuario")
            .leftJoinAndSelect("usuario.pessoa", "pessoa")
            .leftJoinAndSelect("pessoa.pessoaFisica", "pessoaFisica")
            .where("aporte.id = :id", {id: aporteId})
            .getOne();
    }

    @Get('/listaAportes/:usuarioId')
    async listaAportes(@Param('usuarioId') usuarioId : number) {

        const aportes = await this.getAportesPorUsuarioId(usuarioId)
        return {
            aportes
        }
    }

    private async getAportesPorUsuarioId(usuarioId : number) {
        return await getRepository(Aporte)
            .createQueryBuilder("aporte")
            .leftJoinAndSelect("aporte.conta", "conta")
            .leftJoinAndSelect("conta.cliente", "cliente")
            .leftJoinAndSelect("cliente.usuario", "usuario")
            .leftJoinAndSelect("usuario.pessoa", "pessoa")
            .where("usuario.id = :id", {id: usuarioId})
            .orderBy("aporte.dataInicial", "DESC")
            .getMany();
    }
    
    @Get('/listaAportesSolicitados')
    async listaAportesSolicitados() {

        const aportes = await this.getAportesComStatus(this.enumStatusAporte["Solicitado"])
        return {
            aportes
        }
    }
    
    @Get('/listaAportesAguardandoTransferencia')
    async listaAportesAguardandoTransferencia() {

        const aportes = await this.getAportesComStatus(this.enumStatusAporte["Atendido"])
        return {
            aportes
        }
    }
    
    @Get('/listaAportesAguardandoConfirmacaoTranferencia')
    async listaAportesAguardandoConfirmacaoTranferencia() {

        const aportes = await this.getAportesComStatus(this.enumStatusAporte["Transferencia"])
        return {
            aportes
        }
    }
    
    @Get('/listaAportesAtivos')
    async listaAportesAtivos() {

        const aportes = await this.getAportesComStatus(this.enumStatusAporte["Ativo"])
        return {
            aportes
        }
    }
    
    @Get('/listaAportesCancelados')
    async listaAportesCancelados() {

        const aportes = await this.getAportesComStatus(this.enumStatusAporte["Cancelado"])
        return {
            aportes
        }
    }
    
    private async getAportesComStatus(status : number) {
        return await getRepository(Aporte)
            .createQueryBuilder("aporte")
            .leftJoinAndSelect("aporte.conta", "conta")
            .leftJoinAndSelect("conta.cliente", "cliente")
            .leftJoinAndSelect("cliente.usuario", "usuario")
            .leftJoinAndSelect("usuario.pessoa", "pessoa")
            .leftJoinAndSelect("pessoa.pessoaFisica", "pessoaFisica")
            .where("aporte.status = :status", {status: status})
            .getMany();
    }
    
    @Get('/listaAportesOrdenados/:usuarioId/:tipoOrdenacao')
    async listaAportesOrdenados(@Param('usuarioId') usuarioId, @Param('tipoOrdenacao') tipoOrdenacao : number) {
        const aportes = tipoOrdenacao == this.enumTipoOrdencao["DataDecrescente"] ? 
                await this.getAportesAtivosOrdenadosDataDecrescente(usuarioId, tipoOrdenacao)
            : tipoOrdenacao == this.enumTipoOrdencao["DataCrescente"] ? 
                await this.getAportesAtivosOrdenadosDataCrescente(usuarioId, tipoOrdenacao)
            : tipoOrdenacao == this.enumTipoOrdencao["MenorValor"] ? 
                await this.getAportesAtivosOrdenadosMenorValor(usuarioId, tipoOrdenacao)
                : await this.getAportesAtivosOrdenadosMaiorValor(usuarioId, tipoOrdenacao)

        return {
            aportes
        }
    }

    private async getAportesAtivosOrdenadosDataCrescente(usuarioId : number, tipoOrdenacao : number) {
        return await getRepository(Aporte)
            .createQueryBuilder("aporte")
            .leftJoinAndSelect("aporte.conta", "conta")
            .leftJoinAndSelect("conta.cliente", "cliente")
            .leftJoinAndSelect("cliente.usuario", "usuario")
            .leftJoinAndSelect("usuario.pessoa", "pessoa")
            .where("usuario.id = :id", {id: usuarioId})
            .orderBy("aporte.dataInicial", "ASC")
            .getMany();
    }

    private async getAportesAtivosOrdenadosDataDecrescente(usuarioId : number, tipoOrdenacao : number) {
        return await getRepository(Aporte)
            .createQueryBuilder("aporte")
            .leftJoinAndSelect("aporte.conta", "conta")
            .leftJoinAndSelect("conta.cliente", "cliente")
            .leftJoinAndSelect("cliente.usuario", "usuario")
            .leftJoinAndSelect("usuario.pessoa", "pessoa")
            .where("usuario.id = :id", {id: usuarioId})
            .orderBy("aporte.dataInicial", "DESC")
            .getMany();
    }

    private async getAportesAtivosOrdenadosMenorValor(usuarioId : number, tipoOrdenacao : number) {
        return await getRepository(Aporte)
            .createQueryBuilder("aporte")
            .leftJoinAndSelect("aporte.conta", "conta")
            .leftJoinAndSelect("conta.cliente", "cliente")
            .leftJoinAndSelect("cliente.usuario", "usuario")
            .leftJoinAndSelect("usuario.pessoa", "pessoa")
            .where("usuario.id = :id", {id: usuarioId})
            .orderBy("aporte.totalRendimento", "ASC")
            .getMany();
    }

    private async getAportesAtivosOrdenadosMaiorValor(usuarioId : number, tipoOrdenacao : number) {
        return await getRepository(Aporte)
            .createQueryBuilder("aporte")
            .leftJoinAndSelect("aporte.conta", "conta")
            .leftJoinAndSelect("conta.cliente", "cliente")
            .leftJoinAndSelect("cliente.usuario", "usuario")
            .leftJoinAndSelect("usuario.pessoa", "pessoa")
            .where("usuario.id = :id", {id: usuarioId})
            .orderBy("aporte.totalRendimento", "DESC")
            .getMany();
    }

    @Get('/atualizarSaldoAportes')
    async atualizarSaldoAportes(){
        // console.log('ATUALIZANDO APORTE')
        var moment = require('moment');
        let dataAtual = moment(new Date());
        // var listaAportesDoMes = await this.getAportesMesCorrente(dataAtual.month(), dataAtual.year());
        var listaAportesAtivos = await this.getAportesAtivos();

        for(let [index, aporte] of listaAportesAtivos.entries()){
            await this.recalcularSaldoAporte(aporte, dataAtual.month(), dataAtual.year());
        }

        return listaAportesAtivos;
    } 

    private async getAportesAtivos(){
        // return await new Query(Aporte, {connection: getConnection()})
        //     .leftJoinAndSelect().conta().and().cliente().and().usuario().done()
        //     .where().status(StatusAporteEnum.Ativo)
        //     .getMany()
        return await getRepository(Aporte).createQueryBuilder('aporte')
            .leftJoinAndSelect('aporte.conta', 'conta')
            .leftJoinAndSelect('conta.cliente', 'cliente')
            .leftJoinAndSelect('cliente.usuario', 'usuario')
            .andWhere("aporte.status = :status", {status: StatusAporteEnum.Ativo})
            .andWhere(new Brackets(qb => {
                qb.where("aporte.tipo = :tipo", {tipo: TipoAporteEnum.Normal})
                .orWhere(new Brackets(qb => {
                    qb.where("aporte.tipo = :tipo2", {tipo2: TipoAporteEnum.Comissao})
                    .andWhere("usuario.tipoUsuario = :tipoUsuario", {tipoUsuario: TipoUsuarioEnum.Consultor})
                }))
            }))
            .getMany();
    }

    private async recalcularSaldoAporte(aporte : Aporte, mes: number, ano: number){
        var moment = require('moment')
        let dataAtual = moment(new Date())
        let dataAporte = moment(aporte.dataInicial, 'YYYY-MM-DD')
        var diasDoMes = moment(`${ano}-${mes+1}`, 'YYYY-MM').daysInMonth()
        let diferencaMeses = dataAtual.diff(dataAporte, 'months')
        let dataAtualAporte = moment(dataAporte).add(diferencaMeses, 'months')

        if(diferencaMeses > 0 && diferencaMeses > aporte.meses){ // aporte complete one month
            aporte.totalRendimento = aporte.totalRendimento * (1 + Number(aporte.taxaMajoracaoUsuario/100))
            aporte.saldoRendimento = aporte.totalRendimento
            aporte.meses = Number(aporte.meses) + 1
            
            await this.comissaoController.criarComissoes(aporte)
        }else{
            let diasCorridosDoAporte = dataAtual.diff(dataAtualAporte, 'days')
            const rendimentoDiario = Math.pow((1 + (Number(aporte.taxaMajoracaoUsuario)/100)), (1/diasDoMes))
            aporte.saldoRendimento = aporte.totalRendimento * (Math.pow(rendimentoDiario, diasCorridosDoAporte))
        }

        await this.updateAporte(aporte)
    }

    private async updateAporte(aporte : Aporte){
        await getConnection().createQueryBuilder().update(Aporte)
            .set({
                saldoRendimento: aporte.saldoRendimento,
                totalRendimento: aporte.totalRendimento,
                meses: aporte.meses,
            })
            .where("id = :id", { id: aporte.id })
            .execute()
    }

    @Post('alterarStatusAporte')
    async alterarStatusAporte(@Body() message: any, @Headers() headers: any){
        var aporteModel = await getRepository(Aporte)
            .createQueryBuilder("aporte")
            .leftJoinAndSelect("aporte.conta", "conta")
            .leftJoinAndSelect("conta.cliente", "cliente")
            .leftJoinAndSelect("cliente.usuario", "usuario")
            .leftJoinAndSelect("usuario.pessoa", "pessoa")
            .leftJoinAndSelect("pessoa.pessoaFisica", "pessoaFisica")
            .where('aporte.id = :id', {id: message.aporteId})
            .getOne();

        aporteModel.status = this.enumStatusAporte[message.status];
        return aporteModel.save();
    }

    @Post('editDataInicialAporte')
    async editDataInicialAporte(@Body() message: any, @Headers() headers: any){
        var aporteModel = await getRepository(Aporte)
            .createQueryBuilder("aporte")
            .where('aporte.id = :id', {id: message.aporteId})
            .getOne();

        var moment = require('moment');
        aporteModel.dataInicial = moment(message.aporteDataInicial);
        return aporteModel.save();
    }

    // async buscarAportesAbertosEComPorcentagemSobreTaxa(){
    //     return await getRepository(Aporte)
    //         .createQueryBuilder("aporte")
    //         .leftJoinAndSelect("aporte.conta", "conta")
    //         .leftJoinAndSelect("conta.cliente", "cliente")
    //         .leftJoinAndSelect("cliente.usuario", "usuario")
    //         .where("aporte.tipo = :tipo", {tipo: TipoAporteEnum.Aberto})
    //         .andWhere("aporte.tipo = :tipo", {tipo: TipoAporteEnum.SemiAberto})
    //         .andWhere("aporte.status = :status", {status: StatusAporteEnum.Ativo})
    //         .getMany()
    // }

    // async buscarAportesComissaoTI(){
    //     return await getRepository(Aporte)
    //         .createQueryBuilder("aporte")
    //         .leftJoinAndSelect("aporte.conta", "conta")
    //         .leftJoinAndSelect("conta.cliente", "cliente")
    //         .leftJoinAndSelect("cliente.usuario", "usuario")
    //         .where("aporte.tipo = :tipo", {tipo: TipoAporteEnum.Comissao})
    //         .andWhere("usuario.tipoUsuario = :tipoUsuario", {tipoUsuario: TipoUsuarioEnum.TI})
    //         .andWhere("aporte.status = :status", {status: StatusAporteEnum.Ativo})
    //         .getMany()
    // }

    // async calcularRendimentoAportesTotalRendimento(){
    //     var aportesAbertosESemiAbertos = await this.buscarAportesAbertosEComPorcentagemSobreTaxa()
    //     var aportesComissaoTI = await this.buscarAportesComissaoTI()

    //     var taxaTotalRendimento = await this.totalRendimento.getUltimaTaxa()

    //     var moment = require('moment')
    //     let dataAtual = moment(new Date())
        
    //     await this.calcularRendimentoAportesAbertosEComPorcentagemSobreTaxa(aportesAbertosESemiAbertos, dataAtual, taxaTotalRendimento)
    //     await this.calcularRendimentosAportesComissaoTI(aportesComissaoTI, taxaTotalRendimento, dataAtual)
    // }

    /**
     * @description Calcula o rendimento sobre os aportes do tipo aberto, de acordo com o percentual vinculado ao cliente
     * @param aportesAbertosESemiAbertos 
     * @param dataAtual 
     * @param taxaTotalRendimento 
     */
    // private async calcularRendimentoAportesAbertosEComPorcentagemSobreTaxa(aportesAbertosESemiAbertos : Array<Aporte>, dataAtual : any, taxaTotalRendimento : TotalRendimento){
    //     var moment = require('moment')
    //     aportesAbertosESemiAbertos.forEach(aporte => {
    //         let dataAporte = moment(aporte.dataInicial, 'YYYY-MM-DD')
    //         var diasDoMes = moment(`${dataAtual.year()}-${dataAtual.month()+1}`, 'YYYY-MM').daysInMonth()
    //         let diferencaMeses = dataAtual.diff(dataAporte, 'months')
    //         let dataAtualAporte = moment(dataAporte).add(diferencaMeses, 'months')
    //         //para o caso do usuário ser do tipo TI, o rendimento será de 100% sobre a taxa total do mês
    //         let porcentagemRendimento = aporte.conta.cliente.usuario.tipoUsuario == TipoUsuarioEnum.TI ? 1 : Number(aporte.conta.cliente.porcentagemRendimento)/100
            
    //         if(diferencaMeses > 0 && diferencaMeses > aporte.meses){
    //             if(aporte.tipo == TipoAporteEnum.Aberto){
    //                 aporte.totalRendimento = Number(aporte.totalRendimento) * ( Number(taxaTotalRendimento.taxa * porcentagemRendimento)/100 )
    //             }
    //             aporte.saldoRendimento = aporte.totalRendimento
    //             aporte.meses = Number(aporte.meses) + 1
    //         }else{
    //             this.calcularRendimentoPeriodoInferiorAUmMes(dataAtual, dataAtualAporte, aporte, diasDoMes)
    //         }

    //         this.updateAporte(aporte)            
    //     })
    // }

    // private async calcularRendimentosAportesComissaoTI(aportesComissaoTI : Array<Aporte>, taxaTotalRendimento : TotalRendimento, dataAtual : any){
    //     var moment = require('moment')
    //     aportesComissaoTI.forEach(aporte => {
    //         let dataAporte = moment(aporte.dataInicial, 'YYYY-MM-DD')
    //         var diasDoMes = moment(`${dataAtual.year()}-${dataAtual.month()+1}`, 'YYYY-MM').daysInMonth()
    //         let diferencaMeses = dataAtual.diff(dataAporte, 'months')
    //         let dataAtualAporte = moment(dataAporte).add(diferencaMeses, 'months')

    //         if(diferencaMeses > 0 && diferencaMeses > aporte.meses){
    //             aporte.totalRendimento = Number(aporte.totalRendimento) * ( Number(taxaTotalRendimento.taxa)/100 )
    //             aporte.saldoRendimento = aporte.totalRendimento
    //             aporte.meses = Number(aporte.meses) + 1
    //         }else{
    //             this.calcularRendimentoPeriodoInferiorAUmMes(dataAtual, dataAtualAporte, aporte, diasDoMes)
    //         }

    //         this.updateAporte(aporte)    
    //     })
    // }

    // private async calcularRendimentoPeriodoInferiorAUmMes(dataAtual, dataAtualAporte, aporte:Aporte, diasDoMes){
    //     var moment = require('moment')
    //     let diasCorridosDoAporte = dataAtual.diff(dataAtualAporte, 'days')
    //     const rendimentoDiario = Math.pow((1 + (Number(aporte.taxaMajoracaoUsuario)/100)), (1/diasDoMes))
    //     aporte.totalRendimento = aporte.totalRendimento * (Math.pow(rendimentoDiario, diasCorridosDoAporte))
    //     aporte.saldoRendimento = aporte.totalRendimento
    //     aporte.dataInicial = moment(`${dataAtual.year()}-${dataAtual.month()+1}-01`, 'YYYY-MM-DD')
    // }
}
