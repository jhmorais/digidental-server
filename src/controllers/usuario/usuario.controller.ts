import { Controller, Get, Post, Body, Headers, Param } from '@nestjs/common';
import { Usuario } from '../../models/Usuario.entity';
import { getConnection, getRepository, Raw, getManager } from 'typeorm';
import { getEntityManagerToken } from '@nestjs/typeorm';
import { Cliente } from 'src/models/Cliente.entity';
import { Gestor } from 'src/models/Gestor.entity';
import { Consultor } from 'src/models/Consultor.entity';
import { Conta } from 'src/models/Conta.entity';
import * as _ from 'lodash';

@Controller('usuario')
export class UsuarioController {
    private readonly _Gestor: number = 1;
    private readonly _Consultor: number = 2;
    private readonly _Cliente: number = 3;
    private readonly _Financeiro: number = 4;
    private readonly _TI: number = 5;


    async buscarUsuario(login, senha){
        return await getRepository(Usuario).createQueryBuilder().where('usuarioLogin = :login AND usuarioSenha = :senha', {login: login, senha: senha});
    }

    @Get('/usuarioPorNome/:nome')
    async usuarioPorNome(@Param('nome') nome : string) {

        const usuario = await this.getUsuarioPorNome(nome)
        return {
            usuario
        }
    }
    
    @Get('/listaUsuarios')
    async listaUsuarios() {
        const usuarios = await this.getUsuarios()

        // const bcrypt = require('bcrypt');
        // const saltRounds = 10;
        // console.log(bcrypt.hashSync("vitor@prO5p3r!ng", saltRounds));

        const postObject = {
            login: 'usuario',
            senha: 'segredo',
            telefone: '62 ...',
            endereco: 'rua ...'
        }
        this.fillModel(Usuario, postObject)

        return {
            usuarios
        }
    }

    private fillModel<M>(type: {new (): M}, data) {
        const metadata = getConnection().getMetadata(type)
        const instance = new type()
        const properties = metadata.columns.map(c => c.propertyName)
        return _.extend(instance, _.pick(data, properties))
    }

    private async getUsuarios() {
        return await getRepository(Usuario)
        .createQueryBuilder("usuario")
        .leftJoinAndSelect("usuario.pessoa", "pessoa")
        .leftJoinAndSelect("usuario.consultor", "consultor")
        .where('usuario.excluido = :excluido', {excluido: false})
        .orderBy("pessoa.nome", "ASC").getMany();
    }

    private async getUsuarioPorNome(nome : string) {

        return await getRepository(Usuario)
        .createQueryBuilder("usuario")
        .leftJoinAndSelect("usuario.pessoa", "pessoa")
        .where('pessoa.nome ilike :nome AND usuario.excluido = :excluido', {nome: '%' + nome + '%', excluido: false}).getMany();

    }
    
    @Get(':idUsuario')
    async usuarioPorId(@Param('idUsuario') idUsuario) {
        const usuario = await this.getUsuarioPorId(idUsuario)

        return {
            usuario
        }

    }

    private async getUsuarioPorId(id: number) {
        return await Usuario.findOne({
            where: {
                id
            },
            order: {
                login: 'DESC',
            }
        });
    }
    
    @Get('/usuarioCompleto/:idUsuario/:tipoUsuario')
    async usuarioCompleto(@Param('idUsuario') idUsuario, @Param('tipoUsuario') tipoUsuario : number) {
        const usuario = tipoUsuario == this._Gestor ? await this.getUsuarioGestor(idUsuario, tipoUsuario)
                        : tipoUsuario == this._Consultor ? await this.getUsuarioConsultor(idUsuario, tipoUsuario)
                            : await this.getUsuarioCliente(idUsuario, tipoUsuario)

        return {
            usuario
        }

    }

    private async getUsuarioGestor(idUsuario: number, tipoUsuario : number) {
        return await getRepository(Usuario)
        .createQueryBuilder("usuario")
        .leftJoin("usuario.pessoa", "pessoa")
        .leftJoin("usuario.gestor", "gestor")
        .leftJoinAndSelect("usuario.cliente", "cliente")
        .where('usuario.id = :id AND usuario.excluido = :excluido', {id: idUsuario, excluido: false}).getOne();
    }
    
    private async getUsuarioConsultor(idUsuario: number, tipoUsuario : number) {
        return await getRepository(Usuario)
        .createQueryBuilder("usuario")
        .leftJoinAndSelect("usuario.pessoa", "pessoa")
        .leftJoinAndSelect("usuario.cliente", "cliente")
        .leftJoinAndSelect("usuario.consultor", "consultor", "consultor.id = usuario.consultorId")
        .where('usuario.id = :id', {id: idUsuario})
        .andWhere('usuario.excluido = :excluido', {excluido: false})
        .getOne();
    }
    
    private async getUsuarioCliente(idUsuario: number, tipoUsuario : number) {
        return await getRepository(Usuario)
        .createQueryBuilder("usuario")
        .leftJoinAndSelect("usuario.pessoa", "pessoa")
        .leftJoinAndSelect("usuario.cliente", "cliente")
        .leftJoinAndSelect("cliente.consultor", "consultor")
        .where('usuario.id = :id AND usuario.excluido = :excluido', {id: idUsuario, excluido: false}).getOne();
    }
    
    @Get('/usuarioPorCidade/:cidade')
    async usuarioPorCidade(@Param('cidade') cidade) {
        const usuario = await this.getUsuarioPorCidade(cidade)

        return {
            usuario
        }

    }

    private async getUsuarioPorCidade(nome: string) {
        return await getRepository(Usuario)
        .createQueryBuilder("pessoa")
        .leftJoinAndSelect("pessoa.pessoa", "pessoa")
        .leftJoinAndSelect("pessoa.cidade", "cidade")
        .where('cidade.nome ilike :nome AND usuario.excluido = :excluido', {nome: '%' + nome + '%', excluido: false}).getMany();
    }

    @Get()
    async index() {
        const pessoas = await this.usuarios()

        return {
            pessoas
        }

    }

    private async usuarios() {
        return await Usuario.find({
            order: {
                login: 'DESC',
            }
        });
    }

    private async gerarSenha(){
        var generator = require('generate-password');
        return generator.generate({
            length: 8,
            numbers: true,
            symbols: true
        });
    }

    private async gerarSenhaCriptografada(senhaTextoPlano : string){
        const bcrypt = require('bcrypt');
        const saltRounds = 10;
        return bcrypt.hashSync(senhaTextoPlano, saltRounds);
    }

    private async enviarEmailParaUsuario(usuario : Usuario, senhaTextoPlano){
        const nodemailer = require('nodemailer');

        const transporter = nodemailer.createTransport({
            // host: "mail.gmail.com",
            host: "smtp.gmail.com",
            // port: 25,
            port: 465,
            secure: true, // true for 465, false for other ports
            auth: {
                user: "prospering.contato@gmail.com",
                pass: "prospering@33#"
            },
            tls: { rejectUnauthorized: false }
          });

          const mailOptions = {
            from: 'prospering.contato@gmail.com',
            to: 'jhmorais.09@gmail.com, prospering.contato@gmail.com, neivacoelhojr@hotmail.com',
            subject: 'Informações da Prospering',
            text: 'Usuário: ' + usuario.login + '\n Senha: ' + senhaTextoPlano
          };

          transporter.sendMail(mailOptions, function(error, info){
            if (error) {
              console.log(error);
            } else {
              console.log('Email enviado: ' + info.response);
            }
          });
    }
    
    @Post('createUsuario')
    async createUsuario(@Body() message: any, @Headers() headers: any){
        var usuarioModel = new Usuario();
        usuarioModel.login = message.login;
        usuarioModel.pessoaId = message.pessoaId;
        usuarioModel.tipoUsuario = message.tipoUsuario;
        usuarioModel.taxaMajoracao = message.taxaMajoracao == "" ? null : message.taxaMajoracao;
        usuarioModel.excluido = false;
        var senhaTextoPlano = await this.gerarSenha();
        usuarioModel.senha = await this.gerarSenhaCriptografada(senhaTextoPlano);

        if(usuarioModel.tipoUsuario == this._Cliente){
            usuarioModel.consultorId = message.consultorId;
        }
        
        usuarioModel = await getRepository(Usuario).save(usuarioModel);

        if(usuarioModel.tipoUsuario == this._Cliente){
            this.criarCliente(usuarioModel, message)
        }else if(usuarioModel.tipoUsuario == this._Consultor){
            this.criarConsultor(usuarioModel, message.gestorId);
        }else if(usuarioModel.tipoUsuario == this._Gestor){
            this.criarGestor(usuarioModel);
        }

        this.enviarEmailParaUsuario(usuarioModel, senhaTextoPlano);
        return usuarioModel;
    }

    async criarCliente(usuarioModel: Usuario, message : any){
        var clienteModel = new Cliente();
        clienteModel.consultorId = usuarioModel.consultorId;
        clienteModel.usuarioId = usuarioModel.id
        clienteModel.rendimentoVariavel = message.rendimentoVariavel
        clienteModel.porcentagemRendimento = message.porcentagemRendimento
        // clienteModel.fiftyUsuarioId = usuarioModel.id
        clienteModel = await getRepository(Cliente).save(clienteModel);

        await this.setDadosUsuario(usuarioModel, {clienteId: clienteModel.id})
        await this.criarConta(clienteModel);
    }

    async criarConsultor(usuarioModel: Usuario, gestorId: number){
        var consultorModel = new Consultor();
        consultorModel.usuarioId = usuarioModel.id;
        consultorModel.gestorId = gestorId;
        consultorModel = await getRepository(Consultor).save(consultorModel);
        
        var clienteModel = new Cliente();
        clienteModel.usuarioId = usuarioModel.id
        clienteModel.consultorId = consultorModel.id
        clienteModel = await getRepository(Cliente).save(clienteModel); 
        
        await this.setDadosUsuario(usuarioModel, {consultorId: consultorModel.id})
        await this.criarConta(clienteModel);
    }

    async criarGestor(usuarioModel: Usuario){
        var clienteModel = new Cliente();
        clienteModel.usuarioId = usuarioModel.id
        clienteModel = await getRepository(Cliente).save(clienteModel); 

        var gestorModel = new Gestor();
        gestorModel.usuarioId = usuarioModel.id;
        gestorModel = await getRepository(Gestor).save(gestorModel);

        await this.setDadosUsuario(usuarioModel, {gestorId: gestorModel.id})
        await this.criarConta(clienteModel);
    }

    async setDadosUsuario(usuarioModel: Usuario, valuesUpdate : Object){
        await getRepository(Usuario).update(usuarioModel.id, valuesUpdate);
        var usuarioUpdated = await getRepository(Usuario).findOne({ where: { id: usuarioModel.id } });
    }

    async criarConta(clienteModel: Cliente){
        var contaModel = new Conta();
        contaModel.clienteId = clienteModel.id;
        contaModel.saldo = 0;
        contaModel = await getRepository(Conta).save(contaModel);
    }
    
    async excluirRelacoesUsuario(usuario : Usuario, message : any){
        var cliente = await getRepository(Cliente).findOne({where: {usuarioId: message.usuarioId}})
        await getConnection().createQueryBuilder().update(Cliente)
            .set({ excluido: true })
            .where("id = :id", { id: cliente.id })
            .execute();

        if(usuario.consultorId != null)        {
            var consultor = await getRepository(Consultor).findOne({where: {usuarioId: message.usuarioId}})
            await getConnection().createQueryBuilder().update(Consultor)
                .set({ excluido: true })
                .where("id = :id", { id: consultor.id })
                .execute();
        }else if(usuario.gestorId != null){
            var gestor = await getRepository(Gestor).findOne({where: {usuarioId: message.usuarioId}})
            await getConnection().createQueryBuilder().update(Cliente)
                .set({ excluido: true })
                .where("id = :id", { id: gestor.id })
                .execute();
        }
    }

    @Post('deleteUsuario')
    async deleteUsuario(@Body() message: any, @Headers() headers: any){
        // console.log('------------Delete-------------');
        // console.log(message);

        var usuario = await getRepository(Usuario).findOne({where: {id: message.usuarioId}})
        this.excluirRelacoesUsuario(usuario, message);

        await getConnection().createQueryBuilder().update(Usuario)
            .set({ excluido: true })
            .where("id = :id", { id: message.usuarioId })
            .execute();

        return await getRepository(Usuario).findOne({where: {id: message.usuarioId}});
    }
    
    @Post('editUsuario')
    async editUsuario(@Body() message: any, @Headers() headers: any){
        var usuarioAntigo = await getRepository(Usuario).findOne({ where: { id: message.usuarioId } });
        await getRepository(Usuario).update(message.usuarioId, {
            tipoUsuario: message.tipoUsuario, 
            login: message.login, 
            taxaMajoracao: message.taxaMajoracao == '' ? null : message.taxaMajoracao
        });
        var usuario = await getRepository(Usuario).findOne({ where: { id: message.usuarioId } });

        if(usuario.tipoUsuario == usuarioAntigo.tipoUsuario){
            if(usuario.tipoUsuario == this._Cliente){
                console.log('-----------Editando Cliente-------------')
                console.log('message: ', message)
                await getRepository(Cliente).update(usuario.clienteId, {
                    consultorId: message.consultorId,
                    rendimentoVariavel: message.rendimentoVariavel,
                    porcentagemRendimento: message.porcentagemRendimento == "" ? null : message.porcentagemRendimento
                });
            }else if(usuario.tipoUsuario == this._Consultor){
                await getRepository(Consultor).update(usuario.consultorId, {gestorId: message.gestorId})

                let clienteModel = await getRepository(Cliente)
                    .createQueryBuilder('cliente')
                    .where('cliente.usuarioId = :usuarioId', {usuarioId : message.usuarioId})
                    .getOne();
                
                clienteModel.rendimentoVariavel = message.rendimentoVariavel
                clienteModel.porcentagemRendimento = message.porcentagemRendimento
                
                await clienteModel.save()
            }
        }else if(usuario.tipoUsuario != usuarioAntigo.tipoUsuario){
            await this.criarNovasReferenciasTipoUsuario(usuario, message);
            await this.deletarReferenciasAntigasTipoUsuario(usuario, usuarioAntigo);
        }
         
        return usuario;
    }

    async criarNovasReferenciasTipoUsuario(usuario: Usuario, message: any){
        if(usuario.tipoUsuario == this._Gestor){

            var gestorModel = new Gestor();
            gestorModel.usuarioId = usuario.id;
            var gestorNovo = await getRepository(Gestor).save(gestorModel);
            await getRepository(Usuario).update(usuario.id, {gestorId: gestorNovo.id})
        
        }else if(usuario.tipoUsuario == this._Consultor){ 

            var consultorModel = new Consultor();
            consultorModel.usuarioId = usuario.id;
            consultorModel.gestorId = message.gestorId;
            var consultorNovo = await getRepository(Consultor).save(consultorModel);
            await getRepository(Usuario).update(usuario.id, {consultorId: consultorNovo.id})

        }else if(usuario.tipoUsuario == this._Cliente){

            var clienteModel = new Cliente();
            clienteModel.usuarioId = usuario.id;
            clienteModel.consultorId = message.consultorId;
            //Setar o usuario fifty passado
            var consultorFifty = await getRepository(Consultor).findOne({where: {id: message.consultorId}})
            clienteModel.fiftyUsuarioId = consultorFifty.usuarioId

            var clienteNovo = await getRepository(Cliente).save(clienteModel)
            await getRepository(Usuario).update(usuario.id, {clienteId: clienteNovo.id})
        }
    }

    async deletarReferenciasAntigasTipoUsuario(usuario: Usuario, usuarioAntigo: Usuario){
        if(usuarioAntigo.tipoUsuario == this._Cliente){

            await getRepository(Usuario).update(usuario.id, {clienteId: null});
            var usuarioAtualizado = await getRepository(Usuario).findOne({ where: { id: usuario.id } });
            await getConnection().createQueryBuilder().delete().from(Cliente).where("usuarioId = :usuarioId", { usuarioId: usuario.id }).execute();
        
        }else if(usuarioAntigo.tipoUsuario == this._Consultor){

            await getRepository(Usuario).update(usuario.id, {consultorId: null});
            await getConnection().createQueryBuilder().delete().from(Consultor).where("usuarioId = :usuarioId", { usuarioId: usuario.id }).execute();

        }else if(usuarioAntigo.tipoUsuario == this._Gestor){

            await getRepository(Usuario).update(usuario.id, {gestorId: null});
            await getConnection().createQueryBuilder().delete().from(Gestor).where("usuarioId = :usuarioId", { usuarioId: usuario.id }).execute();
        }
    }

    @Post('resetarSenha')
    async resetarSenha(@Body() message: any, @Headers() headers: any){
        var usuario = await getRepository(Usuario).findOne({where: {id: message.usuarioId}})

        var reset = await this.gerarSenhaCriptografada('123@456$789');

        await getConnection().createQueryBuilder().update(Usuario)
            .set({ senha:  reset})
            .where("id = :id", { id: message.usuarioId })
            .execute();

        return await getRepository(Usuario).findOne({where: {id: message.usuarioId}});
    }

    @Post('alterarSenha')
    async alterarSenha(@Body() message: any, @Headers() headers: any){
        var usuario = await getRepository(Usuario).findOne({where: {id: message.usuarioId}})

        var novaSenha = await this.gerarSenhaCriptografada(message.senha);

        await getConnection().createQueryBuilder().update(Usuario)
            .set({ senha: novaSenha })
            .where("id = :id", { id: message.usuarioId })
            .execute();

        return await getRepository(Usuario).findOne({where: {id: message.usuarioId}});
    }
}
