import { Controller, Get, Post, Body, Headers, Param } from '@nestjs/common';
import { getConnection, getRepository, Raw, getManager } from 'typeorm';
import { PessoaFisica } from '../../models/PessoaFisica.entity';

@Controller('pessoa-fisica')
export class PessoaFisicaController {
    @Get('/pessoaFisicaPorCpf/:cpf')
    async pessoaPorNome(@Param('cpf') cpf : string) {

        const pessoa = await this.getPessoaFisicaPorCpf(cpf)

        return {
            pessoa
        }

    }

    private async getPessoaFisicaPorCpf(cpf : string) {

        return await getRepository(PessoaFisica)
        .createQueryBuilder("pessoafisica")
        .leftJoinAndSelect("pessoafisica.pessoa", "pessoa")
        .where('pessoafisica.cpf ilike :cpf', {nome: '%' + cpf + '%'}).getMany();

    }

}
