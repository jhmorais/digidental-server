import { Controller, Get, Post, Param, Body, Headers } from '@nestjs/common';
import { Aporte } from 'src/models/Aporte.entity';
import { TipoAporteEnum } from 'src/enums/tipoAporte.enum';
import { getConnection, getRepository } from 'typeorm';
import { Consultor } from 'src/models/Consultor.entity';
import { HistoricoAporteComissao } from 'src/models/HistoricoAporteComissao.entity';
import { StatusAporteEnum } from 'src/enums/statusAporte.enum';
import { Usuario } from 'src/models/Usuario.entity';
import { TipoUsuarioEnum } from 'src/enums/tipoUsuario.enum';

@Controller('comissao')
export class ComissaoController {

    /**
     * @description Cria comissão para Agentes e TI
     * @param aporte 
     */
    async criarComissoes(aporte : Aporte) {
        await this.criarComissaoAgente(aporte)
        await this.criarComissaoTI(aporte)
    }
    
    /**
     * @description Cria comissão para o agente do cliente dono do aportes do tipo Normal 
     * @param aporte 
     */
    private async criarComissaoAgente(aporte : Aporte) {
        if(aporte.tipo == TipoAporteEnum.Normal){
            var moment = require('moment');
            let dataAtual = moment(new Date()).format('YYYY-MM-DD 00:00:01')
            
            if(aporte.conta.cliente.usuario.tipoUsuario == TipoUsuarioEnum.Cliente){
                var aporteComConsultor = await this.buscarAgenteDoAporte(aporte.id)
                var consultorComConta = await this.buscarConsultorComConta(aporteComConsultor.conta.cliente.consultor.id) // verificar se o consultor tras o usuario correto
                var aporteComissaoAgenteDoDia = await this.buscarComissaoDoDia(consultorComConta.usuario.cliente.conta.id, dataAtual)

                await this.gerenciamentoDeCriacaoDeAporteComissao(aporteComissaoAgenteDoDia, aporte, dataAtual, consultorComConta.usuario)
            }
        }
    }

    /**
     * @description Cria comissão para todos os tipos de aporte (Aberto, Semi Aberto, Normal, Comissao)
     * @param aporte 
     */
    private async criarComissaoTI(aporte : Aporte) {
        var moment = require('moment');
        let dataAtual = moment(new Date()).format('YYYY-MM-DD 00:00:01')
        var usuarioComConta = await this.buscarUsuarioTIComConta()
        var aporteComissaoTIDoDia = await this.buscarComissaoDoDia(usuarioComConta.cliente.conta.id, dataAtual)
        
        await this.gerenciamentoDeCriacaoDeAporteComissao(aporteComissaoTIDoDia, aporte, dataAtual, usuarioComConta)
    }

    /**
     * @description Define se será necessário criar um novo aporte do tipo comissão ou se utiliza um aporte do dia corrente e soma seu valor ao existente
     * @param aporteComissaoDoDia 
     * @param aporte 
     * @param dataAtual 
     * @param usuario 
     */
    private async gerenciamentoDeCriacaoDeAporteComissao(aporteComissaoDoDia:Aporte, aporte:Aporte, dataAtual:Date, usuario:Usuario){
        if(aporteComissaoDoDia != undefined && aporteComissaoDoDia.totalRendimento != null && aporteComissaoDoDia.totalRendimento > 0){
            var historicoAporteComissaoModel = await this.criarHistoricoAporteComissao(aporte, aporteComissaoDoDia, dataAtual)
            aporteComissaoDoDia.totalRendimento = Number(aporteComissaoDoDia.totalRendimento) +  Number(historicoAporteComissaoModel.valor)
            aporteComissaoDoDia.saldoRendimento = aporteComissaoDoDia.totalRendimento
            aporteComissaoDoDia.valor = Number(aporteComissaoDoDia.valor) + Number(historicoAporteComissaoModel.valor)
            aporteComissaoDoDia = await aporteComissaoDoDia.save()
        }else{
            aporteComissaoDoDia = await this.criarAporteComissao(aporteComissaoDoDia, dataAtual, aporte, usuario)
            var historicoAporteComissaoModel = await this.criarHistoricoAporteComissao(aporte, aporteComissaoDoDia, dataAtual)
        }
    }

    /**
     * @description Cria um histórico da origem do valor do aporte comissão do dia  
     * @param aporte 
     * @param aporteComissaoAgenteDoDia 
     * @param dataAtual 
     */
    private async criarHistoricoAporteComissao(aporte, aporteComissaoAgenteDoDia, dataAtual){
        var historicoAporteComissaoModel = new HistoricoAporteComissao()
        historicoAporteComissaoModel.aporteIdComissao = aporteComissaoAgenteDoDia.id
        historicoAporteComissaoModel.aporteIdFonte = aporte.id
        historicoAporteComissaoModel.valor = Number(aporte.totalRendimento) / 100
         historicoAporteComissaoModel.data = dataAtual
        return await historicoAporteComissaoModel.save()
    }

    /**
     * @description Cria o primeiro aporte comissão do dia, com o valor do primeiro aporte com vencimento de 1 mês  
     * @param aporteComissaoAgenteDoDia 
     * @param dataAtual
     * @param aporte
     * @param usuario
     */
    private async criarAporteComissao(aporteComissaoAgenteDoDia:Aporte, dataAtual, aporte, usuario){
        aporteComissaoAgenteDoDia = new Aporte()
        aporteComissaoAgenteDoDia.tipo = TipoAporteEnum.Comissao
        aporteComissaoAgenteDoDia.meses = 0
        aporteComissaoAgenteDoDia.codigo = 0
        aporteComissaoAgenteDoDia.status = StatusAporteEnum.Ativo
        aporteComissaoAgenteDoDia.valor = Number(aporte.totalRendimento) / 100
        aporteComissaoAgenteDoDia.totalRendimento = Number(aporte.totalRendimento) / 100
        aporteComissaoAgenteDoDia.saldoRendimento = Number(aporte.totalRendimento) / 100
        aporteComissaoAgenteDoDia.contaId = usuario.cliente.conta.id
        aporteComissaoAgenteDoDia.taxaMajoracaoUsuario = usuario.tipoUsuario == TipoUsuarioEnum.TI ? null : usuario.taxaMajoracaoUsuario
        aporteComissaoAgenteDoDia.dataInicial = dataAtual
        aporteComissaoAgenteDoDia.dataBase = dataAtual
        aporteComissaoAgenteDoDia.tipo = TipoAporteEnum.Comissao

        aporteComissaoAgenteDoDia = await aporteComissaoAgenteDoDia.save()
        aporteComissaoAgenteDoDia.codigo = Number(aporteComissaoAgenteDoDia.id) + 1364
        return await aporteComissaoAgenteDoDia.save()
    }

    private async buscarComissaoDoDia(idConta:number, dataAtual:Date){
        return await getRepository(Aporte)
        .createQueryBuilder("aporte")
        .where('aporte.tipo = :tipo', {tipo: TipoAporteEnum.Comissao})
        .andWhere("aporte.contaId = :contaId", {contaId: idConta})
        .andWhere("date(aporte.dataInicial) = :dataInicial", {dataInicial: dataAtual})
        .printSql()
        .getOne();
    }

    private async buscarConsultorComConta(idConsultor:number){
        return await getRepository(Consultor)
        .createQueryBuilder("consultor")
        .leftJoinAndSelect("consultor.usuario", "usuario")
        .leftJoinAndSelect("usuario.cliente", "cliente")
        .leftJoinAndSelect("cliente.conta", "conta")
        .leftJoinAndSelect("conta.aporte", "aporte")
        .where('consultor.id = :id', {id: idConsultor})
        .getOne();
    }

    private async buscarAgenteDoAporte(idAporte:number){
        return await getRepository(Aporte)
        .createQueryBuilder("aporte")
        .leftJoinAndSelect("aporte.conta", "conta")
        .leftJoinAndSelect("conta.cliente", "cliente")
        .leftJoinAndSelect("cliente.consultor", "consultor")
        .leftJoinAndSelect("cliente.usuario", "usuario")
        .where('aporte.id = :id', {id: idAporte})
        .getOne();
    }

    @Get('buscarUsuarioTIComConta')
    async buscarUsuarioTIComConta(){
        return await getRepository(Usuario)
        .createQueryBuilder("usuario")
        .leftJoinAndSelect("usuario.cliente", "cliente")
        .leftJoinAndSelect("cliente.conta", "conta")
        .where("usuario.tipoUsuario = :tipoUsuario", {tipoUsuario: TipoUsuarioEnum.TI})
        .getOne()
    }

    /**
     * @description Quando o aporte Aberto, Semi Aberto e Comissao renderam menos de um mês, 
     * a porcentagem da comissão será proporcional ao tempo do aporte
     * @param aporte 
     */
    async criarComissaoTIPorcentagemProporcional(aporte : Aporte, valorRendimentoMesCheio){
        var moment = require('moment');
        let dataAtual = moment(new Date()).format('YYYY-MM-DD 00:00:01')
        var usuarioComConta = await this.buscarUsuarioTIComConta()
        var aporteComissaoTIDoDia = await this.buscarComissaoDoDia(usuarioComConta.cliente.conta.id, dataAtual)
        
        await this.gerenciamentoDeCriacaoDeAporteComissaoProporcional(aporteComissaoTIDoDia, aporte, dataAtual, usuarioComConta, valorRendimentoMesCheio)
    }

    /**
     * @description Define se será necessário criar um novo aporte do tipo comissão ou se utiliza um aporte do dia corrente e soma seu valor ao existente
     * @param aporteComissaoDoDia 
     * @param aporte 
     * @param dataAtual 
     * @param usuario 
     */
    private async gerenciamentoDeCriacaoDeAporteComissaoProporcional(aporteComissaoDoDia:Aporte, aporte:Aporte, dataAtual:Date, usuario:Usuario, valorRendimentoMesCheio){
        if(aporteComissaoDoDia != undefined && aporteComissaoDoDia.totalRendimento != null && aporteComissaoDoDia.totalRendimento > 0){
            var historicoAporteComissaoModel = await this.criarHistoricoAporteComissaoProporcional(aporte, aporteComissaoDoDia, dataAtual, valorRendimentoMesCheio)
            aporteComissaoDoDia.totalRendimento = Number(aporteComissaoDoDia.totalRendimento) +  Number(historicoAporteComissaoModel.valor)
            aporteComissaoDoDia.saldoRendimento = aporteComissaoDoDia.totalRendimento
            aporteComissaoDoDia.valor = Number(aporteComissaoDoDia.valor) + Number(historicoAporteComissaoModel.valor)
            aporteComissaoDoDia = await aporteComissaoDoDia.save()
        }else{
            aporteComissaoDoDia = await this.criarAporteComissaoProporcional(aporteComissaoDoDia, dataAtual, aporte, usuario, valorRendimentoMesCheio)
            var historicoAporteComissaoModel = await this.criarHistoricoAporteComissaoProporcional(aporte, aporteComissaoDoDia, dataAtual, valorRendimentoMesCheio)
        }
    }

    /**
     * @description Cria um histórico da origem do valor do aporte comissão do dia  
     * @param aporte 
     * @param aporteComissaoAgenteDoDia 
     * @param dataAtual 
     */
    private async criarHistoricoAporteComissaoProporcional(aporte, aporteComissaoAgenteDoDia, dataAtual, valorRendimentoMesCheio){
        var historicoAporteComissaoModel = new HistoricoAporteComissao()
        historicoAporteComissaoModel.aporteIdComissao = aporteComissaoAgenteDoDia.id
        historicoAporteComissaoModel.aporteIdFonte = aporte.id
        // valor da comissao será proporcional ao tempo de rendimento do aporte 
        // valor rendimento proporcional elevado a 2, divido pelo rendimento do mes cheio vezes 100
        historicoAporteComissaoModel.valor = Math.pow(Number(aporte.totalRendimento), 2) / (Number(valorRendimentoMesCheio) * 100)
        historicoAporteComissaoModel.data = dataAtual
        return await historicoAporteComissaoModel.save()
    }

    /**
     * @description Cria o primeiro aporte comissão do dia, com o valor do primeiro aporte com vencimento de 1 mês  
     * @param aporteComissaoAgenteDoDia 
     * @param dataAtual
     * @param aporte
     * @param usuario
     */
    private async criarAporteComissaoProporcional(aporteComissaoAgenteDoDia:Aporte, dataAtual, aporte, usuario, valorRendimentoMesCheio){
        aporteComissaoAgenteDoDia = new Aporte()
        aporteComissaoAgenteDoDia.tipo = TipoAporteEnum.Comissao
        aporteComissaoAgenteDoDia.meses = 0
        aporteComissaoAgenteDoDia.codigo = 0
        aporteComissaoAgenteDoDia.status = StatusAporteEnum.Ativo
        aporteComissaoAgenteDoDia.valor = Math.pow(Number(aporte.totalRendimento), 2) / (Number(valorRendimentoMesCheio) * 100)
        aporteComissaoAgenteDoDia.totalRendimento = Math.pow(Number(aporte.totalRendimento), 2) / (Number(valorRendimentoMesCheio) * 100)
        aporteComissaoAgenteDoDia.saldoRendimento = Math.pow(Number(aporte.totalRendimento), 2) / (Number(valorRendimentoMesCheio) * 100)
        aporteComissaoAgenteDoDia.contaId = usuario.cliente.conta.id
        aporteComissaoAgenteDoDia.taxaMajoracaoUsuario = usuario.tipoUsuario == TipoUsuarioEnum.TI ? null : usuario.taxaMajoracaoUsuario
        aporteComissaoAgenteDoDia.dataInicial = dataAtual
        aporteComissaoAgenteDoDia.dataBase = dataAtual
        aporteComissaoAgenteDoDia.tipo = TipoAporteEnum.Comissao

        aporteComissaoAgenteDoDia = await aporteComissaoAgenteDoDia.save()
        aporteComissaoAgenteDoDia.codigo = Number(aporteComissaoAgenteDoDia.id) + 1364
        return await aporteComissaoAgenteDoDia.save()
    }

}
