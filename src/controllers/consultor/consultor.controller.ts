import { Controller, Get, Param } from '@nestjs/common';
import { Consultor } from 'src/models/Consultor.entity';
import { getRepository } from 'typeorm';

@Controller('consultor')
export class ConsultorController {
    
    @Get('/listaConsultores')
    async listaConsultores() {

        const consultores = await this.getConsultores()
        return {
            consultores
        }
    }

    private async getConsultores() {
        return await getRepository(Consultor)
        .createQueryBuilder("consultor")
        .leftJoinAndSelect("consultor.usuario", "usuario")
        .leftJoinAndSelect("usuario.pessoa", "pessoa")
        .orderBy('pessoa.nome').getMany();
    }

    @Get('/consultorPorUsuarioId/:usuarioId')
    async cidadePorNome(@Param('usuarioId') usuarioId : number) {

        const consultor = await this.getConsultorPorUsuarioId(usuarioId)
        
        return {
            consultor
        }

    }

    private async getConsultorPorUsuarioId(usuarioId : number) {
        return await getRepository(Consultor)
        .createQueryBuilder("consultor")
        .leftJoinAndSelect("consultor.usuario", "usuario")
        .leftJoinAndSelect("usuario.pessoa", "pessoa")
        .where('consultor.usuarioId = :usuarioId', {usuarioId: usuarioId}).getOne();
    }
}
