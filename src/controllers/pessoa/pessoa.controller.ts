import { Controller, Get, Post, Body, Headers, Param } from '@nestjs/common';
import { Pessoa } from '../../models/Pessoa.entity';
import { getConnection, getRepository, Raw, getManager } from 'typeorm';
import { getEntityManagerToken } from '@nestjs/typeorm';
import { Usuario } from 'src/models/Usuario.entity';
import { PessoaFisica } from 'src/models/PessoaFisica.entity';

@Controller('pessoa')
export class PessoaController {

    @Get('/pessoaPorNome/:nome')
    async pessoaPorNome(@Param('nome') nome : string) {

        const pessoa = await this.getPessoaPorNome(nome)
        return {
            pessoa
        }
    }
    
    @Get('/listaPessoas')
    async listaPessoas() {

        const pessoas = await this.getPessoas()
        return {
            pessoas
        }
    }

    private async getPessoas() {
        return await getRepository(Pessoa)
        .createQueryBuilder("pessoa")
        .leftJoinAndSelect("pessoa.pessoaFisica", "pessoaFisica")
        .orderBy("pessoa.nome", "ASC")
        .getMany();
    }

    @Get('/listaPessoasSemUsuario')
    async listaPessoasSemUsuario() {

        const pessoas = await this.getPessoasSemUsuario()
        return {
            pessoas
        }
    }

    private async getPessoasSemUsuario() {
        var listaPessoas = await getRepository(Pessoa)
        .createQueryBuilder("pessoa")
        .leftJoinAndSelect("pessoa.pessoaFisica", "pessoaFisica")
        .leftJoinAndSelect("pessoa.usuario", "usuario")
        .where("usuario.id is null")
        .orderBy("pessoa.nome", "ASC")
        .getMany();

        // return listaPessoas
        return listaPessoas.filter(pes => pes.usuario == null || pes.usuario.excluido == true)
    }

    private async getPessoaPorNome(nome : string) {

        return await getRepository(Pessoa)
        .createQueryBuilder("pessoa")
        .leftJoinAndSelect("pessoa.cidade", "cidade")
        .where('pessoa.nome ilike :nome', {nome: '%' + nome + '%'}).getMany();

    }
    
    @Get(':idPessoa')
    async pessoaPorId(@Param('idPessoa') idPessoa) {
        const pessoa = await this.getPessoaPorId(idPessoa)

        return {
            pessoa
        }

    }

    private async getPessoaPorId(id: number) {
        return await getRepository(Pessoa)
        .createQueryBuilder("pessoa")
        .leftJoinAndSelect("pessoa.pessoaFisica", "pessoaFisica")
        .where("pessoa.id = :id", {id: id})
        .getOne();
    }
    
    @Get('/pessoaPorCidade/:cidade')
    async pessoaPorCidade(@Param('cidade') cidade) {

        const pessoa = await this.getPessoaPorCidade(cidade)

        return {
            pessoa
        }

    }

    private async getPessoaPorCidade(nome: string) {
        return await getRepository(Pessoa)
        .createQueryBuilder("pessoa")
        .leftJoinAndSelect("pessoa.cidade", "cidade")
        .where('cidade.nome ilike :nome', {nome: '%' + nome + '%'}).getMany();
    }

    @Get('/pessoaPorUsuarioId/:usuarioId')
    async pessoaPorUsuarioId(@Param('usuarioId') usuarioId) {
        const pessoa = await this.getPessoaPorUsuarioId(usuarioId)

        return {
            pessoa
        }

    }

    private async getPessoaPorUsuarioId(usuarioId: number) {
        return await getRepository(Pessoa)
        .createQueryBuilder("pessoa")
        .leftJoinAndSelect("pessoa.pessoaFisica", "pessoaFisica")
        .leftJoinAndSelect("pessoa.usuario", "usuario")
        .leftJoinAndSelect("pessoa.cidade", "cidade")
        .leftJoinAndSelect("cidade.uf", "uf")
        .where("usuario.id = :usuarioId", {usuarioId: usuarioId})
        .getOne();
    }

    @Get()
    async index() {

        const pessoas = await this.pessoas()

        return {
            pessoas
        }

    }

    private async pessoas() {
        return await Pessoa.find({
            order: {
                nome: 'DESC',
            }
        });
    }
    
    /*
        URL => http://app:3300/pessoa/createPessoa
        Headers => Content-Type: application/json
        Raw:
        {
            "nome": "Antônio Coelho",
            "bairro": "Parque das Riquezas",
            "cep": 74410000,
            "cidadeId": 1114,
            "complemento": "qd. 15 lt. 35",
            "email": "antonio@prospering.com.net",
            "endereco": "Rua dos Trilhões",
            "estadoCivil": 2,
            "telefone": "3241-1266",
            "numero": 1169,
            "profissao": "Fazedor de Dinheiro",
            "status": true
        }
    */
    private bindPessoa(message: any){
        var pessoaModel = new Pessoa();
        pessoaModel.nome = message.nome;
        pessoaModel.cidadeId = message.cidadeId;
        pessoaModel.profissao = message.profissao;
        pessoaModel.telefone = message.telefone;
        pessoaModel.endereco = message.endereco;
        pessoaModel.cep = message.cep;
        pessoaModel.complemento = message.complemento;
        pessoaModel.numero = message.numero;
        pessoaModel.bairro = message.bairro;
        pessoaModel.email = message.email;
        pessoaModel.status = message.status;
        pessoaModel.estadoCivil = message.estadoCivil;
        return pessoaModel;
    }

    @Post('createPessoa')
    async createPessoa(@Body() message: any, @Headers() headers: any){
        var pessoaModel = this.bindPessoa(message);

        pessoaModel = await getRepository(Pessoa).save(pessoaModel)

        var pessoaFisicaModel = new PessoaFisica();
        pessoaFisicaModel.pessoaId = pessoaModel.id;
        pessoaFisicaModel.cpf = message.cpf;
        pessoaFisicaModel = await getRepository(PessoaFisica).save(pessoaFisicaModel);

        return pessoaModel;
    }

    @Post('editarPessoaPerfil')
    async editarPessoaPerfil(@Body() message: any, @Headers() headers: any){
        var pessoaModel = new Pessoa();
        pessoaModel.id = message.pessoa.id;
        pessoaModel.nome = message.pessoa.nome;
        pessoaModel.cidadeId = message.pessoa.cidadeId;
        pessoaModel.profissao = message.pessoa.profissao;
        pessoaModel.telefone = message.pessoa.telefone.replace(/\D/g, '');
        pessoaModel.endereco = message.pessoa.endereco;
        pessoaModel.cep = message.pessoa.cep.replace(/\D/g, '');
        pessoaModel.complemento = message.pessoa.complemento;
        pessoaModel.numero = message.pessoa.numero;
        pessoaModel.bairro = message.pessoa.bairro;
        pessoaModel.email = message.pessoa.email;
        pessoaModel.status = message.pessoa.status;
        pessoaModel.estadoCivil = message.pessoa.estadoCivil;

        pessoaModel = await getRepository(Pessoa).save(pessoaModel)

        var pessoaFisicaModel = new PessoaFisica();
        pessoaFisicaModel.pessoaId = pessoaModel.id;
        pessoaFisicaModel.cpf = message.cpf;
        pessoaFisicaModel = await getRepository(PessoaFisica).save(pessoaFisicaModel);

        return pessoaModel;
    }

    @Post('deletePessoa')
    async deletePessoa(@Body() message: any, @Headers() headers: any){
        
        var usuario = await getRepository(Usuario)
        .createQueryBuilder("usuario")
        .leftJoinAndSelect("usuario.pessoa", "pessoa")
        .where('usuario.pessoaId = :pessoaId', {pessoaId: message.pessoaId})
        .andWhere("usuario.excluido = :excluido", {excluido: false})
        .getOne();
        
        await this.excluirPessoaFisica(message);
        
        if(usuario == null){
            return await getConnection()
                .createQueryBuilder()
                .delete()
                .from(Pessoa)
                .where("id = :id", { id: message.pessoaId })
                .execute();
        }else{
            return {
                status: "Fail",
                message: "Pessoa selecionada para excluir possui um usuário vinculado"
            }
        }

    }

    async excluirPessoaFisica(message : any){
        var pessoaFisicaModel = await getRepository(PessoaFisica)
        .createQueryBuilder("pessoaFisica")
        .where('pessoaFisica.pessoaId = :pessoaId', {pessoaId: message.pessoaId})
        .getOne();

        if(pessoaFisicaModel != null){
            await getConnection()
                .createQueryBuilder()
                .delete()
                .from(PessoaFisica)
                .where("id = :id", { id: pessoaFisicaModel.id })
                .execute();
        }
    }
    
    @Post('editPessoa')
    async editPessoa(@Body() message: any, @Headers() headers: any){
        await getRepository(Pessoa).update(message.pessoaId, {
            nome: message.nome, 
            email: message.email,
            telefone: message.telefone
        });

        var pessoaAtualizada = await getRepository(Pessoa)
            .createQueryBuilder("pessoa")
            .leftJoinAndSelect("pessoa.pessoaFisica", "pessoaFisica")
            .where("pessoa.id = :pessoaId", {pessoaId: message.pessoaId})
            .getOne();
        
        var pessoaFisicaModel = pessoaAtualizada.pessoaFisica != null ? pessoaAtualizada.pessoaFisica : new PessoaFisica();
        pessoaFisicaModel.pessoaId = pessoaAtualizada.id;
        pessoaFisicaModel.cpf = message.cpf;
        pessoaFisicaModel.nascimento = message.dataNascimento;
        pessoaFisicaModel = await getRepository(PessoaFisica).save(pessoaFisicaModel);

        return pessoaAtualizada;
    }
    
}
