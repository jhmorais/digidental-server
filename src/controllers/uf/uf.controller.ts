import { Controller, Get, Post, Param, Body, Headers } from '@nestjs/common';
import { getConnection, getRepository } from 'typeorm';
import { Uf } from 'src/models/Uf.entity';

@Controller('uf')
export class UfController {

    @Get('/estadoPorNome/:nome')
    async estadoPorNome(@Param('nome') nome : string) {
        const estado = await this.getEstadoPorNome(nome)

        return {
            estado
        }

    }

    private async getEstadoPorNome(nome : string) {
        return await getRepository(Uf)
        .createQueryBuilder("uf")
        .where('uf.nome ilike :nome', {nome: '%' + nome + '%'}).getMany();
    }

    @Get('/estadoPorId/:id')
    async estadoPorId(@Param('id') id : number) {
        const estado = await this.getEstadoPorId(id)

        return {
            estado
        }

    }

    private async getEstadoPorId(id : number) {
        return await getRepository(Uf)
        .createQueryBuilder("uf")
        .where('uf.id = :id', {id: id}).getOne();
    }

    @Get()
    async index() {
        const estados = await this.estados()
        return {
            estados
        }

    }

    private async estados() {
        return await Uf.find({
            order: {
                nome: 'ASC',
            }
        });
    }
    
}
