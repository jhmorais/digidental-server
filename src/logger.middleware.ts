import { Injectable, NestMiddleware, HttpException } from '@nestjs/common';
import { Request, Response } from 'express';
import { JwtService } from '@nestjs/jwt';
import { throwError } from 'rxjs';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {

    constructor(private readonly jwtService: JwtService) {}

    use(req: Request, res: Response, next: Function) {
        // console.log('=============================================================');
        // console.log("GET em acao");
        if(req.method == "GET"){
            next();
        }else{
            // console.log('Request...');
            // console.log(req.method);
            // console.log('=============================================================');
            // console.log(req.method == "GET");

            const token = req.header('Authorization')

            // console.log(token)
            // console.log(token == '')
            // console.log(req.body['userName'])
            // console.log(req.body['password'])

            if(token == '' && req.body['userName'] != null && req.body['password'] != null){
                next();
            }else if(token != ''){
                // console.log('=============================================================');
                // console.log(this.jwtService.decode(token)['exp'])

                if (Date.now() >= parseFloat(this.jwtService.decode(token)['exp']) * 1000) {
                    // console.log('=============================================================');
                    // console.log('Expirado');
                    throw new HttpException("Autenticacao expirada", 500);
                }else{
                    this.jwtService.verify(token)
                    next();
                }
            }else{
                return {"status": 500, "message": "Acesso não permitido"};
            }
        }

        
        

    }
}
