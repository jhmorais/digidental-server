import {BaseEntity, Entity, PrimaryColumn, Column, OneToOne, JoinColumn, getConnection, createQueryBuilder} from "typeorm";
import {Usuario} from "./Usuario.entity";
import {Perfil} from "./Perfil.entity";

@Entity({schema: 'desenvolvimento', name: 'perfilusuario'})
export class PerfilUsuario extends BaseEntity {
    
    @PrimaryColumn({name: 'perfilusuario_id'})
    id: number

    @Column({name: 'perfilusuario_perfil_id'})
    perfilId: number

    @Column({name: 'perfilusuario_usuario_id'})
    usuarioId: number

    @OneToOne(type => Perfil)
    @JoinColumn({
        name: "perfilusuario_perfil_id",
        referencedColumnName: "id"
    })
    perfil: Perfil;

    @OneToOne(type => Usuario)
    @JoinColumn({
        name: "perfilusuario_usuario_id",
        referencedColumnName: "id"
    })
    usuario: Usuario;

}