import {BaseEntity, Entity, PrimaryColumn, Column, OneToOne, JoinColumn, getConnection, createQueryBuilder, PrimaryGeneratedColumn} from "typeorm";
import {Pessoa} from "./Pessoa.entity";

@Entity({schema: 'desenvolvimento', name: 'pessoafisica'})
export class PessoaFisica extends BaseEntity {
    
    @PrimaryGeneratedColumn({name: 'pessoafisica_id'})
    id: number

    @Column({name: 'pessoafisica_cpf'})
    cpf: number

    @Column({name: 'pessoafisica_nascimento'})
    nascimento: Date

    @Column({name: 'pessoafisica_pessoa_id'})
    pessoaId: number
    
    @OneToOne(type => Pessoa)
    @JoinColumn({
        name: "pessoafisica_pessoa_id",
        referencedColumnName: "id"
    })
    pessoa: Pessoa;

}