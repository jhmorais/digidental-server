import {BaseEntity, Entity, PrimaryColumn, Column, OneToOne, JoinColumn, getConnection, createQueryBuilder, PrimaryGeneratedColumn} from "typeorm";
import {Usuario} from "./Usuario.entity";
import { Gestor } from "./Gestor.entity";

@Entity({schema: 'desenvolvimento', name: 'consultor'})
export class Consultor extends BaseEntity {
    
    @PrimaryGeneratedColumn({name: 'consultor_id'})
    id: number

    @Column({name: 'consultor_usuario_id'})
    usuarioId: number
    
    @Column({name: 'consultor_gestor_id'})
    gestorId: number
    
    @Column({name: 'consultor_excluido'})
    excluido: boolean
    
    @OneToOne(type => Usuario)
    @JoinColumn({
        name: "consultor_usuario_id",
        referencedColumnName: "id"
    })
    usuario: Usuario;
    
    @OneToOne(type => Gestor)
    @JoinColumn({
        name: "consultor_gestor_id",
        referencedColumnName: "id"
    })
    gestor: Gestor;

}