import {BaseEntity, Entity, PrimaryColumn, Column, OneToOne, JoinColumn, getConnection, createQueryBuilder, PrimaryGeneratedColumn} from "typeorm";

@Entity({schema: 'desenvolvimento', name: 'perfil'})
export class Perfil extends BaseEntity {
    
    @PrimaryGeneratedColumn({name: 'perfil_id'})
    id: number

    @Column({name: 'perfil_nome'})
    nome: number

}