import {BaseEntity, Entity, PrimaryColumn, Column, OneToOne, JoinColumn, getConnection, createQueryBuilder, PrimaryGeneratedColumn} from "typeorm";
import { Aporte } from "./Aporte.entity";

@Entity({schema: 'desenvolvimento', name: 'historicoaportecomissao'})
export class HistoricoAporteComissao extends BaseEntity {
    
    @PrimaryGeneratedColumn({name: 'historico_id'})
    id: number

    @Column({name: 'historico_aporte_id_fonte'})
    aporteIdFonte: number

    @Column({name: 'historico_aporte_id_comissao'})
    aporteIdComissao: number

    @Column({name: 'historico_data'})
    data: Date

    @Column({name: 'historico_valor'})
    valor: number

    @OneToOne(type => Aporte)
    @JoinColumn({
        name: "historico_aporte_id_fonte",
        referencedColumnName: "id"
    })
    aporteFonte: Aporte;

}