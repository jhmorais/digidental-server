import {BaseEntity, Entity, PrimaryColumn, Column, OneToOne, JoinColumn, getConnection, createQueryBuilder, PrimaryGeneratedColumn} from "typeorm";
import {Usuario} from "./Usuario.entity";
import {Consultor} from "./Consultor.entity";
import { Conta } from "./Conta.entity";

@Entity({schema: 'desenvolvimento', name: 'cliente'})
export class Cliente extends BaseEntity {
    
    @PrimaryGeneratedColumn({name: 'cliente_id'})
    id: number

    @Column({name: 'cliente_usuario_id'})
    usuarioId: number

    @Column({name: 'cliente_consultor_id'})
    consultorId: number

    @Column({name: 'cliente_fifty_usuario_id'})
    fiftyUsuarioId: number

    @Column({name: 'cliente_excluido'})
    excluido: boolean

    @Column({name: 'cliente_rendimento_variavel'})
    rendimentoVariavel: boolean

    @Column({name: 'cliente_porcentagem_rendimento'})
    porcentagemRendimento: number
    
    @OneToOne(type => Usuario)
    @JoinColumn({
        name: "cliente_usuario_id",
        referencedColumnName: "id"
    })
    usuario: Usuario;
    
    @OneToOne(type => Consultor)
    @JoinColumn({
        name: "cliente_consultor_id",
        referencedColumnName: "id"
    })
    consultor: Consultor;

    @OneToOne(type => Conta, conta => conta.cliente)
    conta: Conta;

}