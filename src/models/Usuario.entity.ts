import {BaseEntity, Entity, PrimaryColumn, Column, OneToOne, JoinColumn, getConnection, createQueryBuilder, PrimaryGeneratedColumn} from "typeorm";
import {Pessoa} from "./Pessoa.entity";
import { Consultor } from "./Consultor.entity";
import { Cliente } from "./Cliente.entity";
import { Gestor } from "./Gestor.entity";

@Entity({schema: 'desenvolvimento', name: 'usuario'})
export class Usuario extends BaseEntity {
    
    @PrimaryGeneratedColumn({name: 'usuario_id'})
    id: number

    @Column({name: 'usuario_login'})
    login: string
    
    @Column({name: 'usuario_senha'})
    senha: string
    
    @Column({name: 'usuario_token_senha'})
    tokenSenha: string
    
    @Column({name: 'usuario_token_email'})
    tokenEmail: string
    
    @Column({name: 'usuario_pessoa_id'})
    pessoaId: number
    
    @Column({name: 'usuario_tipo_usuario'})
    tipoUsuario: number
    
    @Column({name: 'usuario_cliente_id'})
    clienteId: number
    
    @Column({name: 'usuario_consultor_id'})
    consultorId: number
    
    @Column({name: 'usuario_gestor_id'})
    gestorId: number
    
    @Column({name: 'usuario_taxa_majoracao'})
    taxaMajoracao: number
    
    @Column({name: 'usuario_excluido'})
    excluido: boolean
    
    @OneToOne(type => Pessoa)
    @JoinColumn({
        name: "usuario_pessoa_id",
        referencedColumnName: "id"
    })
    pessoa: Pessoa;
    
    @OneToOne(type => Gestor)
    @JoinColumn({
        name: "usuario_gestor_id",
        referencedColumnName: "id"
    })
    gestor: Gestor;
    
    @OneToOne(type => Consultor)
    @JoinColumn({
        name: "usuario_consultor_id",
        referencedColumnName: "id"
    })
    consultor: Consultor;
    
    @OneToOne(type => Cliente)
    @JoinColumn({
        name: "usuario_cliente_id",
        referencedColumnName: "id"
    })
    cliente: Cliente;

}