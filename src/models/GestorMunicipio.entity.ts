import {BaseEntity, Entity, PrimaryColumn, Column, getConnection} from "typeorm";

@Entity({schema: 'saudegestao', name: 'gestores_mun'})
export class GestorMunicipio extends BaseEntity {
    
    @PrimaryColumn({name: 'gmun_codibge'})
    codigoIbge: number

    @Column({name: 'gmun_prefeito'})
    prefeito: string

    @Column({name: 'gmun_secretario'})
    secretario: string

    
    /*static repo() {
        return getConnection().getRepository(GestorMunicipio)
    }*/
}