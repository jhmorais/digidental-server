import {BaseEntity, Entity, PrimaryColumn, Column, OneToOne, JoinColumn, getConnection, createQueryBuilder, PrimaryGeneratedColumn} from "typeorm";

@Entity({schema: 'desenvolvimento', name: 'menu'})
export class Menu extends BaseEntity {
    
    @PrimaryGeneratedColumn({name: 'menu_id'})
    id: number

    @Column({name: 'menu_texto'})
    texto: string

    @Column({name: 'menu_posicao'})
    posicao: number

    @Column({name: 'menu_icone'})
    icone: string

}