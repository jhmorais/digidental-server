import {BaseEntity, Entity, PrimaryColumn, Column, OneToOne, JoinColumn, getConnection, createQueryBuilder, PrimaryGeneratedColumn} from "typeorm";
import {Pessoa} from "./Pessoa.entity";

@Entity({schema: 'desenvolvimento', name: 'pessoajuridica'})
export class Cidade extends BaseEntity {
    
    @PrimaryGeneratedColumn({name: 'pessoajuridica_id'})
    id: number

    @Column({name: 'pessoajuridica_cnpj'})
    cpf: number

    @Column({name: 'pessoajuridica_contato'})
    nascimento: Date

    @Column({name: 'pessoajuridica_pessoa_id'})
    pessoaId: number
    
    @OneToOne(type => Pessoa)
    @JoinColumn({
        name: "pessoajuridica_pessoa_id",
        referencedColumnName: "id"
    })
    pessoa: Pessoa;

}