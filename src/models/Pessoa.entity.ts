import {BaseEntity, Entity, PrimaryColumn, Column, OneToOne, JoinColumn, getConnection, createQueryBuilder, PrimaryGeneratedColumn, JoinTable} from "typeorm";
import {Cidade} from "./Cidade.entity";
import { PessoaFisica } from "./PessoaFisica.entity";
import { Usuario } from "./Usuario.entity";

@Entity({schema: 'desenvolvimento', name: 'pessoa'})
export class Pessoa extends BaseEntity {
    
    @PrimaryGeneratedColumn({name: 'pessoa_id'})
    id: number

    @Column({name: 'pessoa_nome'})
    nome: string
    
    @Column({name: 'pessoa_profissao', nullable: true})
    profissao?: string | null
    
    @Column({name: 'pessoa_fone'})
    telefone: string
    
    @Column({name: 'pessoa_endereco', nullable: true})
    endereco?: string | null
    
    @Column({name: 'pessoa_cep', nullable: true})
    cep?: number | null
    
    @Column({name: 'pessoa_complemento', nullable: true})
    complemento?: string | null
    
    @Column({name: 'pessoa_numero', nullable: true})
    numero?: number | null
    
    @Column({name: 'pessoa_bairro', nullable: true})
    bairro?: string | null
    
    @Column({name: 'pessoa_email'})
    email: string
    
    @Column({name: 'pessoa_status', nullable: true})
    status?: number | null
    
    @Column({name: 'pessoa_estado_civil', nullable: true})
    estadoCivil?: number | null
    
    @Column({name: 'pessoa_cidade_id', nullable: true})
    cidadeId?: number | null
    
    @OneToOne(type => Cidade)
    @JoinColumn({
        name: "pessoa_cidade_id",
        referencedColumnName: "id"
    })
    cidade: Cidade;

    @OneToOne(type => PessoaFisica, pessoaFisica => pessoaFisica.pessoa)
    pessoaFisica: PessoaFisica;

    @OneToOne(type => Usuario, usuario => usuario.pessoa)
    usuario: Usuario;

}