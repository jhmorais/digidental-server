import {BaseEntity, Entity, PrimaryColumn, Column, OneToOne, JoinColumn, getConnection, createQueryBuilder} from "typeorm";
import {Uf} from "./Uf.entity";

@Entity({schema: 'desenvolvimento', name: 'cidade'})
export class Cidade extends BaseEntity {
    
    @PrimaryColumn({name: 'id'})
    id: number

    @Column({name: 'nome'})
    nome: string

    @Column({name: 'uf_id'})
    ufId: number
    
    @OneToOne(type => Uf)
    @JoinColumn({
        name: "uf_id",
        referencedColumnName: "id"
    })
    uf: Uf;

}