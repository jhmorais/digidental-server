import {BaseEntity, Entity, PrimaryColumn, Column, OneToOne, JoinColumn, getConnection, createQueryBuilder, PrimaryGeneratedColumn} from "typeorm";
import {Menu} from "./Menu.entity";

@Entity({schema: 'desenvolvimento', name: 'itemmenu'})
export class ItemMenu extends BaseEntity {
    
    @PrimaryGeneratedColumn({name: 'itemmenu_id'})
    id: number

    @Column({name: 'itemmenu_texto'})
    texto: string

    @Column({name: 'itemmenu_link'})
    link: string

    @Column({name: 'itemmenu_permissoes'})
    permissoes: string

    @Column({name: 'itemmenu_posicao'})
    posicao: number

    @Column({name: 'itemmenu_menu_id'})
    menuId: number

    @OneToOne(type => Menu)
    @JoinColumn({
        name: "itemmenu_menu_id",
        referencedColumnName: "id"
    })
    menu: Menu;

}