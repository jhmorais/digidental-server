import {BaseEntity, Entity, PrimaryColumn, Column, getConnection, createQueryBuilder} from "typeorm";

@Entity({schema: 'saudegestao', name: 'dengue_incidencia'})
export class DengueIncidencia extends BaseEntity {
    
    @PrimaryColumn({name: 'dinc_codibge'})
    codigoIbge: number

    @Column({name: 'dinc_ano'})
    ano: number

    @Column({name: 'dinc_semana'})
    semana: number

    @Column({name: 'dinc_populacao'})
    populacao: number

    @Column({name: 'dinc_casos'})
    casos: number

    @Column({name: 'dinc_classificacao'})
    classificacao: string
    
    @Column({name: 'dinc_incidencia'})
    incidencia: number
}