import {BaseEntity, Entity, PrimaryColumn, Column, OneToOne, JoinColumn, getConnection, createQueryBuilder, PrimaryGeneratedColumn, ManyToOne, OneToMany} from "typeorm";
import {Conta} from "./Conta.entity";
import { Resgate } from "./Resgate.entity";

@Entity({schema: 'desenvolvimento', name: 'aporte'})
export class Aporte extends BaseEntity {
    
    @PrimaryGeneratedColumn({name: 'aporte_id'})
    id: number

    @Column({name: 'aporte_codigo'})
    codigo: number

    @Column({name: 'aporte_data_inicial'})
    dataInicial: Date

    @Column({name: 'aporte_data_base'})
    dataBase: Date

    @Column({name: 'aporte_conta_id'})
    contaId: number

    @Column({name: 'aporte_valor'})
    valor: number
    
    @Column({name: 'aporte_status'})
    status: number
    
    @Column({name: 'aporte_saldo_rendimento'})
    saldoRendimento: number
    
    @Column({name: 'aporte_total_rendimento'})
    totalRendimento: number
    
    @Column({name: 'aporte_meses'})
    meses: number
    
    @Column({name: 'aporte_taxa_majoracao_usuario'})
    taxaMajoracaoUsuario: number
    
    @Column({name: 'aporte_tipo'})
    tipo: number
    
    @ManyToOne(type => Conta)
    @JoinColumn({
        name: "aporte_conta_id",
        referencedColumnName: "id"
    })
    conta: Conta;

    @OneToMany(type => Resgate, resgate => resgate.aporte)
    resgates: Resgate[];

}