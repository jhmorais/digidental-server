import {BaseEntity, Entity, PrimaryColumn, Column, OneToOne, JoinColumn, getConnection, createQueryBuilder, PrimaryGeneratedColumn, ManyToOne} from "typeorm";
import {Aporte} from "./Aporte.entity";

@Entity({schema: 'desenvolvimento', name: 'resgate'})
export class Resgate extends BaseEntity {
    
    @PrimaryGeneratedColumn({name: 'resgate_id'})
    id: number

    @Column({name: 'resgate_aporte_id'})
    aporteId: number

    @Column({name: 'resgate_data'})
    data: Date

    @Column({name: 'resgate_valor'})
    valor: number

    @Column({name: 'resgate_valor_aporte_antes_resgate'})
    valorAporteAntesDoResgate: number
    
    @Column({name: 'resgate_mes'})
    mes: number
    
    @ManyToOne(type => Aporte)
    @JoinColumn({
        name: "resgate_aporte_id",
        referencedColumnName: "id"
    })
    aporte: Aporte;

}