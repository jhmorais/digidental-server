import {BaseEntity, Entity, PrimaryColumn, Column, getConnection, createQueryBuilder} from "typeorm";

@Entity({schema: 'desenvolvimento', name: 'uf'})
export class Uf extends BaseEntity {
    
    @PrimaryColumn({name: 'id'})
    id: number

    @Column({name: 'nome'})
    nome: string

}