import {BaseEntity, Entity, PrimaryColumn, Column, OneToOne, JoinColumn, getConnection, createQueryBuilder, PrimaryGeneratedColumn} from "typeorm";
import {Usuario} from "./Usuario.entity";

@Entity({schema: 'desenvolvimento', name: 'gestor'})
export class Gestor extends BaseEntity {
    
    @PrimaryGeneratedColumn({name: 'gestor_id'})
    id: number

    @Column({name: 'gestor_usuario_id'})
    usuarioId: number

    @Column({name: 'gestor_excluido'})
    excluido: boolean
    
    @OneToOne(type => Usuario)
    @JoinColumn({
        name: "gestor_usuario_id",
        referencedColumnName: "id"
    })
    usuario: Usuario;

}