import {BaseEntity, Entity, PrimaryColumn, Column, OneToOne, JoinColumn, getConnection, createQueryBuilder} from "typeorm";
import { ItemMenu } from "./ItemMenu.entity";
import { Perfil } from "./Perfil.entity";

@Entity({schema: 'desenvolvimento', name: 'itemmenuperfil'})
export class ItemMenuPerfil extends BaseEntity {
    
    @PrimaryColumn({name: 'itemmenuperfil_id'})
    id: number

    @Column({name: 'itemmenuperfil_permissoes'})
    permissoes: string

    @Column({name: 'itemmenuperfil_perfil_id'})
    perfilId: number

    @Column({name: 'itemmenuperfil_itemmenu_id'})
    itemMenuId: number

    @OneToOne(type => ItemMenu)
    @JoinColumn({
        name: "itemmenuperfil_itemmenu_id",
        referencedColumnName: "id"
    })
    itemMenu: ItemMenu;

    @OneToOne(type => Perfil)
    @JoinColumn({
        name: "itemmenuperfil_perfil_id",
        referencedColumnName: "id"
    })
    perfil: Perfil;

}