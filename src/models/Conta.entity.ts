import {BaseEntity, Entity, PrimaryColumn, Column, OneToOne, JoinColumn, getConnection, createQueryBuilder, PrimaryGeneratedColumn} from "typeorm";
import {Cliente} from "./Cliente.entity";
import { Usuario } from "./Usuario.entity";
import { Aporte } from "./Aporte.entity";

@Entity({schema: 'desenvolvimento', name: 'conta'})
export class Conta extends BaseEntity {
    
    @PrimaryGeneratedColumn({name: 'conta_id'})
    id: number

    @Column({name: 'conta_saldo'})
    saldo: number

    @Column({name: 'conta_cliente_id'})
    clienteId: number
    
    @OneToOne(type => Cliente)
    @JoinColumn({
        name: "conta_cliente_id",
        referencedColumnName: "id"
    })
    cliente: Cliente;

    @OneToOne(type => Aporte, aporte => aporte.conta)
    aporte: Aporte;

}