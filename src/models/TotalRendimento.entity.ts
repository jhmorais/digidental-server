import {BaseEntity, Entity, PrimaryColumn, Column, OneToOne, JoinColumn, getConnection, createQueryBuilder, PrimaryGeneratedColumn} from "typeorm";

@Entity({schema: 'desenvolvimento', name: 'totalrendimento'})
export class TotalRendimento extends BaseEntity {
    
    @PrimaryGeneratedColumn({name: 'totalrendimento_id'})
    id: number

    @Column({name: 'totalrendimento_mes'})
    mes: number

    @Column({name: 'totalrendimento_ano'})
    ano: number

    @Column({name: 'totalrendimento_taxa'})
    taxa: number

    @Column({name: 'totalrendimento_data_lancamento'})
    dataLancamento: Date

}