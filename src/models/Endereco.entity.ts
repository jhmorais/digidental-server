import {BaseEntity, Entity, PrimaryColumn, Column, OneToOne, JoinColumn, getConnection, createQueryBuilder, PrimaryGeneratedColumn, JoinTable} from "typeorm";
import {Cidade} from "./Cidade.entity";
import { Usuario } from "./Usuario.entity";

@Entity({schema: 'desenvolvimento', name: 'endereco'})
export class Endereco extends BaseEntity {
    
    @PrimaryGeneratedColumn({name: 'id'})
    id: number

    @Column({name: 'logradouro'})
    logradouro: string
    
    @Column({name: 'cep', nullable: true})
    cep?: number | null
    
    @Column({name: 'numero', nullable: true})
    numero?: number | null
    
    @Column({name: 'setor', nullable: true})
    setor?: string | null
    
    @Column({name: 'cidade_id', nullable: true})
    cidadeId?: number | null
    
    @OneToOne(type => Cidade)
    @JoinColumn({
        name: "cidade_id",
        referencedColumnName: "id"
    })
    cidade: Cidade;

}