import {BaseEntity, Entity, PrimaryColumn, Column, OneToOne, JoinColumn, getConnection, createQueryBuilder, PrimaryGeneratedColumn, JoinTable} from "typeorm";
import {Cidade} from "./Cidade.entity";
import { Usuario } from "./Usuario.entity";
import { Endereco } from "./Endereco.entity";

@Entity({schema: 'desenvolvimento', name: 'paciente'})
export class Paciente extends BaseEntity {
    
    @PrimaryGeneratedColumn({name: 'id'})
    id: number

    @Column({name: 'nome'})
    nome: string

    @Column({name: 'cpf'})
    cpf: string

    @Column({name: 'rg'})
    rg: string

    @Column({name: 'sexo'})
    sexo: string
    
    @Column({name: 'profissao', nullable: true})
    profissao?: string | null
    
    @Column({name: 'telefone'})
    telefone: string
    
    @Column({name: 'orgao_expeditor'})
    orgaoExpeditor: string
    
    @Column({name: 'celular'})
    celular: string
    
    @Column({name: 'conjugue'})
    conjugue: string
    
    @Column({name: 'endereco_id', nullable: true})
    enderecoId?: number | null
    
    @Column({name: 'nacionalidade', nullable: true})
    nacionalidade?: string | null
    
    @Column({name: 'naturalidade_cidade_id', nullable: true})
    naturalidadeCidadeId?: number | null
    
    @Column({name: 'endereco_profissional_id', nullable: true})
    enderecoProfissionalId?: number | null
    
    @Column({name: 'email'})
    email: string
    
    @Column({name: 'estado_civil', nullable: true})
    estadoCivil?: number | null

    @Column({name: 'responsavel_paciente_id', nullable: true})
    responsavelPacienteId?: number | null

    @Column({name: 'data_nascimento'})
    dataNascimento: Date

    @Column({name: 'excluido'})
    excluido: boolean

    @Column({name: 'parentesco'})
    parentesco: string

    @Column({name: 'contato_emergencia'})
    contatoEmergencia: string
    
    @OneToOne(type => Endereco)
    @JoinColumn({
        name: "endereco_id",
        referencedColumnName: "id"
    })
    endereco: Endereco;
    
    @OneToOne(type => Endereco)
    @JoinColumn({
        name: "endereco_profissional_id",
        referencedColumnName: "id"
    })
    enderecoProfissional: Endereco;
    
    @OneToOne(type => Paciente)
    @JoinColumn({
        name: "responsavel_paciente_id",
        referencedColumnName: "id"
    })
    responsavelPaciente: Paciente;
    
    @OneToOne(type => Cidade)
    @JoinColumn({
        name: "naturalidade_cidade_id",
        referencedColumnName: "id"
    })
    naturalidadeCidade: Cidade;

}