import {BaseEntity, Entity, PrimaryColumn, Column, OneToOne, JoinColumn, getConnection, createQueryBuilder, PrimaryGeneratedColumn} from "typeorm";
import {Conta} from "./Conta.entity";

@Entity({schema: 'desenvolvimento', name: 'saque'})
export class Saque extends BaseEntity {
    
    @PrimaryGeneratedColumn({name: 'saque_id'})
    id: number

    @Column({name: 'saque_valor'})
    valor: number

    @Column({name: 'saque_data'})
    data: Date

    @Column({name: 'saque_conta_id'})
    contaId: number

    @OneToOne(type => Conta)
    @JoinColumn({
        name: "saque_conta_id",
        referencedColumnName: "id"
    })
    conta: Conta;

}