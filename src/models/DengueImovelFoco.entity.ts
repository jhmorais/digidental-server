import {BaseEntity, Entity, PrimaryColumn, Column, getConnection, createQueryBuilder} from "typeorm";

@Entity({schema: 'saudegestao', name: 'dengue_infest_imov_foco'})
export class DengueImovelFoco extends BaseEntity {
    
    @PrimaryColumn({name: 'diif_codibge'})
    codigoIbge: number

    @Column({name: 'diif_ano'})
    ano: number

    @Column({name: 'diif_mes'})
    mes: number

    @Column({name: 'diif_percent'})
    percent: number

    @Column({name: 'diif_classificacao'})
    classificacao: number

}