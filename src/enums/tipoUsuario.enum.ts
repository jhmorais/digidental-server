export enum TipoUsuarioEnum {
    Gestor = 1,
    Consultor = 2,
    Cliente = 3,
    Financeiro = 4,
    TI = 5,
  }