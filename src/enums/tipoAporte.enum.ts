export enum TipoAporteEnum {
    Normal = 1,
    Aberto = 2,
    SemiAberto = 3,
    Comissao = 4,
  }