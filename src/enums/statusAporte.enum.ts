export enum StatusAporteEnum {
    Solicitado = 1,
    Atendido = 2,
    Transferencia = 3,
    Ativo = 4,
    Inativo = 5,
    Falha = 6,
    Cancelado = 7
  }