import { Controller, Get, Request, Post, UseGuards } from '@nestjs/common';
import { AppService } from './app.service';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth/auth.service';

@Controller('api')
export class AppController {
  constructor(private readonly appService: AppService, private readonly authService: AuthService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  // @UseGuards(AuthGuard('local'))
  @Post('login')
  async login(@Request() req) {
    var user = {userName: req.body.userName, password: req.body.password}
    return this.authService.login(user);
  }

  @Post('validarUsuario')
  async validarUsuario(@Request() req) {
    return this.authService.validateToken(req.body.access_token);
  }
}
