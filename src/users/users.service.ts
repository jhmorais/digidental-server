import { Injectable } from '@nestjs/common';
import { getRepository } from 'typeorm';
import { Usuario } from 'src/models/Usuario.entity';

export type User = any;

@Injectable()
export class UsersService {
  private readonly users: User[];

  constructor() {
    this.users = [];
  }

  async findOne(username: string): Promise<User | undefined> {
    return Usuario.findOne({
        where: {
            login: username
        }
    });
  }
}