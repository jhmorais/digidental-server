const express = require('express');
const app = express();
const path = require('path');
const port = process.env.PORT || 3000;
const rateLimit = require('express-rate-limit');
const helmet = require('helmet')

//Static file declaration
app.use(express.static(path.join(__dirname, '../public')));

//production mode
// if(process.env.NODE_ENV === 'production') {
//   app.use(express.static(path.join(__dirname, 'client/build')));
//   //
//   app.get('*', (req, res) => {
//     res.sendfile(path.join(__dirname = 'client/build/index.html'));
//   })
// }
//build mode
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname+'../public/index.html'));
})

//start server
app.listen(port, (req, res) => {
  console.log( `server listening on port: ${port}`);
})

app.use('/static', express.static(path.join(__dirname, '../public')))

app.use(helmet())

app.use(
  rateLimit({
    windowMs: 15 * 60 * 1000, // 15 minutes
    max: 20, // limit each IP to 20 requests per windowMs
  }),
);